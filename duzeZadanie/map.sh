#!/bin/bash
FILE="$1"

if ! [[ $FILE =~ ^\/ ]]
then
    FILE="/$1" 
fi

if [[ $# = 0 ]] || [[ ! -f "$FILE" ]]
then
    exit 1
fi

for i in "${@:2}"
do
    if ! [[ $i =~ ^[1-9][0-9]{,2}$ ]]
    then
        exit 1
    fi

    while IFS= read -r line
    do
        if [[ $i = "${line%%;*}" ]]
        then
            var=0
            line=${line#*;}
            while [[ $line =~ \;.+\; ]]
            do
                line=${line#*;}
                var=$((var + ${line%%;*}))
                line=${line#*;}
                line=${line#*;}
            done
            echo "$i;$var"
        fi
    done < "$FILE"
done
