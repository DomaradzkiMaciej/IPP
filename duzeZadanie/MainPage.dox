/**
@mainpage Dokumentacja zadania drogi

### Treść zadania

Uwaga: aktualna treść zadania znajduje się w [Moodle'u](https://moodle.mimuw.edu.pl).

### Opis programu

Tegoroczne duże zadanie polega na zaimplementowaniu operacji na mapach drogowych.

Program przy uruchomieniu tworzy jedną pustą mapę, a po zakończeniu ją usuwa.

Dostępne opracje na mapie i ich składnia:

* addRoad;city1;city2;length;builtYear - polecenie dodaje do mapy odcinek drogi między dwoma podanymi miastami
  o podanej długości. Jeśli któreś z podanych miast nie istnieje, to dodaje go do mapy, a następnie dodaje do mapy
  odcinek drogi między tymi miastami. Polecenie jest błędne, jeśli któryś z parametrów ma niepoprawną wartość, obie
  podane nazwy miast są identyczne lub odcinek drogi między tymi miastami już istnieje. To polecenie niczego nie
  wypisuje na standardowe wyjście.

* repairRoad;city1;city2;repairYear - polecenie modyfikuje rok ostatniego remontu odcinka drogi. Dla odcinka drogi
  między dwoma miastami zmienia rok jego ostatniego remontu lub ustawia ten rok, jeśli odcinek nie był jeszcze
  remontowany. Polecenie jest błędne, jeśli któryś z parametrów ma niepoprawną wartość, któreś z podanych miast nie
  istnieje, nie ma odcinka drogi między podanymi miastami, podany rok jest wcześniejszy niż zapisany dla tego odcinka
  drogi rok budowy lub ostatniego remontu. To polecenie niczego nie wypisuje na standardowe wyjście.

* numer drogi krajowej;nazwa miasta;długość odcinka drogi;rok budowy lub ostatniego remontu;nazwa miasta;;…;nazwa miasta
  - polecenie tworzy drogę krajową o podanym numerze i przebiegu. Jeśli jakieś miasto lub odcinek drogi nie istnieje,
  to go tworzy. Jeśli odcinek drogi już istnieje, ale ma wcześniejszy rok budowy lub ostatniego remontu, to modyfikuje
  ten atrybut odcinka drogi. Polecenie jest błędne, jeśli istnieje już droga krajowa o podanym numerze, jedna z podanych
  nazw miasta się powtarza ,jakiś odcinek drogi już istnieje, ale ma inną długość albo późniejszy rok budowy
  lub ostatniego remontu. To polecenie niczego nie wypisuje na standardowe wyjście.

* newRoute;routeId;city1;city2 - polecenie tworzy drogę krajową pomiędzy dwoma miastami i nadaje jej podany numer.
  Wśród istniejących odcinków dróg wyszukuje najkrótszą drogę. Jeśli jest więcej niż jeden sposób takiego wyboru, to
  dla każdego wariantu wyznacza wśród wybranych w nim odcinków dróg ten, który był najdawniej wybudowany lub remontowany
  i wybiera wariant z odcinkiem, który jest najmłodszy. Polecenie jest błędne, jeśli któryś z parametrów ma niepoprawną
  wartość, istnieje już droga krajowa o podanym numerze, któreś z podanych miast nie istnieje, obie podane nazwy miast
  są identyczne, nie można jednoznacznie wyznaczyć drogi krajowej między podanymi miastami. To polecenie niczego nie
  wypisuje na standardowe wyjście.

* extendRoute;routeId;city - polecenie wydłuża drogę krajową do podanego miasta. Dodaje do drogi krajowej nowe odcinki
  dróg do podanego miasta w taki sposób, aby nowy fragment drogi krajowej był najkrótszy. Jeśli jest więcej niż jeden
  sposób takiego wydłużenia, to dla każdego wariantu wyznacza wśród dodawanych odcinków dróg ten, który był najdawniej
  wybudowany lub remontowany i wybiera wariant z odcinkiem, który jest najmłodszy. Polecenie jest błędne, jeśli któryś
  z parametrów ma niepoprawną nazwę, nie istnieje droga krajowa o podanym numerze, nie ma miasta o podanej nazwie,
  przez podane miasto już przechodzi droga krajowa o podanym numerze, podana droga krajowa kończy się w podanym mieście,
  lub nie można jednoznacznie wyznaczyć nowego fragmentu drogi krajowej. To polecenie niczego nie wypisuje na
  standardowe wyjście.

* removeRoad;city1;city2 - polecenie usuwa odcinek drogi między dwoma różnymi miastami. Jeśli usunięcie tego odcinka
  drogi powoduje przerwanie ciągu jakiejś drogi krajowej, to uzupełnia ją istniejącymi odcinkami dróg w taki sposób, aby
  była najkrótsza. Jeśli jest więcej niż jeden sposób takiego uzupełnienia, to dla każdego wariantu wyznacza wśród
  dodawanych odcinków drogi ten, który był najdawniej wybudowany lub remontowany i wybiera wariant z odcinkiem,
  który jest najmłodszy. Polecenie jest błędne, jeśli z powodu błędu nie można usunąć tego odcinka drogi: któryś
  z parametrów ma niepoprawną wartość, nie ma któregoś z podanych miast, nie istnieje droga między podanymi miastami,
  lub nie da się jednoznacznie uzupełnić przerwanego ciągu drogi krajowej To polecenie niczego nie wypisuje na
  standardowe wyjście.

* removeRoute;routeId - polecenie usuwa z mapy drogę krajową o podanym numerze. Polecenie jest błędne, jeśli nie istieje
  droga krajowa o podany numerze. To polecenie niczego nie wypisuje na standardowe wyjście.

* getRouteDescription;routeId - polecenie wypisuje informacje o drodze krajowej. Informacje wypisywane są w formacie:
  numer drogi krajowej;nazwa miasta;długość odcinka drogi;rok budowy lub ostatniego remontu;nazwa miasta;długość odcinka
  drogi;rok budowy lub ostatniego remontu;nazwa miasta;…;nazwa miasta. Polecenie jest błędne, jeśli nie istieje droga
  krajowa o podany numerze i nie wypisuje wtedy niczego na standardowe wyjście.

Obsługa błędów
   Jeśli polecenie jest niepoprawne składniowo lub jego wykonanie zakończyło się błędem, to funkcja wypisuje na
   standardowe wyjście diagnostyczne jednoliniowy komunikat ERROR n, gdzie n jest numerem linii w danych wejściowych
   zawierającym to polecenie.

*/
