#ifndef _INPUT_H
#define _INPUT_H

#include <stdbool.h>

/**
 * Funkcja wczytuje jedną linię ze standartowego wejścia.
 * @return wskaźnik na napis zawierający wczytaną linię lub NULL
 * jeśli nie udało się zaalokować pamięci.
 */
char *getLine();
/**
 * Funkcja sprawdza czy podana linia zawiera poprawną komendę dodawania drogi
 * i jeśli tak to zapisuje osobno nazwy miast, długość i rok budowy.
 * @param line - wczytana linia ze standartowego wejścia
 * @param city1 - podwójny wskażnik na tablicę znaków do której zostanie wpisana nazwa 1. miasta, o ile jest
 * to komenda na dodanie drogi
 * @param city2 - podwójny wskażnik na tablicę znaków do której zostanie wpisana nazwa 2. miasta, o ile jest
 * to komenda na dodanie drogi
 * @param length - wskażnik na unsigned int do którego zostanie przypisana długość drogi, o ile jest to komenda
 * na dodanie drogi
 * @param built_year - wskażnik na int do którego zostanie przypisany rok budowy drogi, o ile jest to komenda
 * na dodanie drogi
 * @return true, jeśli linia zawiera poprawną komendę dodawania drogi i false w przeciwnym wypadku.
 */
bool isItAddRoad( const char *line, char **city1, char **city2, unsigned *length, int *built_year);
/**
 * Funkcja sprawdza czy podana linia zawiera poprawną komendę naprawiania drogi
 * i jeśli tak to zapisuje osobno nazwy miast i rok budowy.
 * @param line - wczytana linia ze standartowego wejścia
 * @param city1 - podwójny wskażnik na tablicę znaków, do której zostanie wpisana nazwa 1. miasta, o ile jest
 * to komenda na naprawienie drogi
 * @param city2 - podwójny wskażnik na tablicę znaków, do której zostanie wpisana nazwa 2. miasta, o ile jest
 * to komenda na naprawienie drogi
 * @param repair_year - wskażnik na int do którego zostanie przypisany rok naprawy drogi, o ile jest to komenda
 * na naprawienie drogi
 * @return true, jeśli linia zawiera poprawną komendę naprawiania drogi i false w przeciwnym wypadku.
 */
bool isItRepairRoad( const char *line, char **city1, char **city2, int *repair_year);
/**
 * Funkcja sprawdza czy podana linia zawiera poprawną komendę na dostanie opisu drogi krajowej
 * i jeśli tak, to zapisuje osobno jej numer.
 * @param line - wczytana linia ze standartowego wejścia
 * @param routeId - wskażnik na unsigned int do którego zostanie przypisany numer drogi, o ile jest to komenda
 * na dostanie opisu drogi krajowej
 * @return true, jeśli linia zawiera poprawną komendę na dostanie opisu drogi krajowej
 * i false w przeciwnym wypadku.
 */
bool isItGetRouteDescription( const char *line, unsigned *routeId);
/**
 * Funkcja sprawdza czy podana linia zawiera poprawną komendę stworzenia drogi krajowej
 * i jeśli tak to zapisuje osobno jej miasto początkowe, kolejne miasta, numer drogi krajowej,
 * długości poszczególnych odcinków i lata budowy poszczególnych odcinków.
 * @param line - wczytana linia ze standartowego wejścia
 * @param routeId - wskażnik na unsigned int do którego zostanie przypisany numer drogi, o ile jest to komenda na
 * dodanie drogi krajowej przez podane miasta
 * @param city - podwójny wskażnik na tablicę znaków, do której zostanie wpisana nazwa pierwszego miasta na drodze,
 * o ile jest to komenda na naprawienie drogi
 * @param cities_on_the_route - podwójny wskażnik na tablicę wskażników na tablice znaków do których zostają wpisane
 * nazwy drugiego i kolejnych miast na drodze, o ile jest to komenda na dodanie drogi krajowej przez podane miasta
 * @param lengths_of_route_sections - podwójny wskażnik na tablicę unsignedd intów do których zostają przypisane
 * długości posczególnych odcinków drogi krajowek, o ile jest to komenda na dodanie drogi krajowej przez podane miasta
 * @param years_of_route_sections - podwójny wskażnik na tablicę intów do których zostają przypisane lata budowy
 * posczególnych odcinków drogi krajowek, o ile jest to komenda na dodanie drogi krajowej przez podane miasta
 * @param length_of_arrays - wskażnik na int do którego zostanie przypisanie przypisana długość 3 powyższych tablic,
 * o ile jest to komenda na dodanie drogi krajowej przez podane miasta
 * @return true, jeśli linia zawiera poprawną komendę stworzenia drogi krajowej
 * i false w przeciwnym wypadku.
 */
bool isItNewRouteByGivenCities( const char *line, unsigned *routeId, char **city, char ***cities_on_the_route,
        unsigned **lengths_of_route_sections, int **years_of_route_sections, int *length_of_arrays);
/**
 * Funkcja sprawdza czy podana linia zawiera poprawną komendę na dodanie drogi krajowej
 * i jeśli tak, to zapisuje osobno jej numer oraz miasta startowe i końcowe.
 * @param line - wczytana linia ze standartowego wejścia
 * @param routeId - wskażnik na unsigned int do którego zostanie przypisany numer drogi, o ile jest to komenda
 * na dodanie drogi krajowej
 * @param city1 - podwójny wskażnik na tablicę znaków, do której zostanie wpisana nazwa 1. miasta, o ile jest
 * to komenda na dodanie drogi krajowej
 * @param city2 - podwójny wskażnik na tablicę znaków, do której zostanie wpisana nazwa 2. miasta, o ile jest
 * to komenda na dodanie drogi krajowej
 * @return true, jeśli linia zawiera poprawną komendę na dodanie drogi krajowej
 * i false w przeciwnym wypadku.
 */
bool isItNewRoute( const char *line, unsigned *routeId, char **city1, char **city2);
/**
 * Funkcja sprawdza czy podana linia zawiera poprawną komendę na wydłłużenie drogi krajowej do podanego miasta
 * i jeśli tak, to zapisuje osobno jej numer oraz miasto do którego ma być wydłużona.
 * @param line - wczytana linia ze standartowego wejścia
 * @param routeId - wskażnik na unsigned int do którego zostanie przypisany numer drogi, o ile jest to komenda
 * na wydłużenie drogi krajowej
 * @param city - podwójny wskażnik na tablicę znaków, do której zostanie wpisana nazwa miasta, o ile jest
 * to komenda na wydłużenie drogi krajowej
 * @return true, jeśli linia zawiera poprawną komendę na wydłużenie drogi krajowej
 * i false w przeciwnym wypadku.
 */
bool isItExtendRoute( const char *line, unsigned *routeId, char **city);
/**
 * Funkcja sprawdza czy podana linia zawiera poprawną komendę na usunięcie drogi między dwoma miastami
 * i jeśli tak, to zapisuje osobno miasta pomiędzy którymi prowadzi usuwaną droga.
 * @param line - wczytana linia ze standartowego wejścia
 * @param city1 - podwójny wskażnik na tablicę znaków, do której zostanie wpisana nazwa 1. miasta, o ile jest
 * to komenda na usunięcie drogi
 * @param city2 - podwójny wskażnik na tablicę znaków, do której zostanie wpisana nazwa 2. miasta, o ile jest
 * to komenda na usunięcie drogi
 * @return true, jeśli linia zawiera poprawną komendę na usunięcie drogi
 * i false w przeciwnym wypadku.
 */
bool isItRemoveRoad( const char *line, char **city1, char **city2);
/**
 * Funkcja sprawdza czy podana linia zawiera poprawną komendę na usunięcie drogi krajowej
 * i jeśli tak, to zapisuje osobno jej numer.
 * @param line - wczytana linia ze standartowego wejścia
 * @param routeId - wskażnik na unsigned int do którego zostanie przypisany numer drogi, o ile jest to komenda
 * na usunięcie drogi krajowej
 * @return true, jeśli linia zawiera poprawną komendę na usunięcie drogi krajowej
 * i false w przeciwnym wypadku.
 */
bool isItRemoveRoute( const char *line, unsigned *routeId);

#endif // _INPUT_H
