#include "input.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <values.h>

char *getLine(){

    char *line = malloc(2 * sizeof(char));
    char character;
    int length_of_line = 0;
    int size_of_line = 2;

    for (int i=0; (character = (char) getchar()) != '\n'; i++){
        length_of_line++;

        if (length_of_line > size_of_line) {
            size_of_line = 2 * size_of_line;
            line = realloc(line, size_of_line * sizeof(char));

            if (NULL == line)
                return NULL;
        }

        line[i] = character;

        if (EOF == character)
            break;
    }

    length_of_line++;

    if (length_of_line >= size_of_line) {
        size_of_line = 2 * size_of_line;
        line = realloc(line, size_of_line * sizeof(char));
    }

    if (NULL == line)
        return NULL;

    line[length_of_line - 1] = '\0';

    return line;
}
/**
 * Funkcja sprawdza, czy w podanym stringu od podanej pozycji do średnika lub do końca napisu znajduje się
 * poprawna nazwa miasta, w szczególności czy jest co najmniej długości 1.
 * @param line - wczytana linia ze standartowego wejścia
 * @param position_of_city - pozycja w napisie od którego zaczyna się nazwa miasta
 * @return true, jeśli nazwa jest poprawna i false w przeciwnym wypadku.
 */
static bool isNameCorrect( const char *line, int position_of_city){

    if (line[position_of_city] == ';' || line[position_of_city] == '\0')
        return false;

    for (int i = position_of_city; line[i] != '\0' && line[i] != ';' ; ++i)
        if (line[i] >= 0 && line[i] <= 31)
            return false;

    return true;
}
/**
 * Funkcja sprawdza, czy w podanym stringu od podanej pozycji do średnika lub do końca napisu znajduje się
 * poprawna nazwa miasta, w szczególności czy jest co najmniej długości 1 i jeśli jest, to wczytuje ją do
 * podanego stringa.
 * @param line - wczytana linia ze standartowego wejścia
 * @param position_of_city - pozycja w napisie od którego zaczyna się nazwa miasta
 * @param city - wskażnik na wskażnik na tablice charów
 * @return true, jeśli nazwa została wczytana i false w przeciwnym wypadku.
 */
static bool getCityName( const char *line, int position_of_city, char **city){

    if (false == isNameCorrect( line, position_of_city))
        return false;

    *city = realloc(*city, 2 * sizeof(char));

    int length_of_name = 0;
    int size_of_name = 2;

    for (int i = position_of_city; line[i] != ';' && line[i] != '\0'; ++i){
        length_of_name++;

        if (length_of_name > size_of_name){
            size_of_name = 2 * size_of_name;
            *city = realloc(*city, size_of_name * sizeof(char));

            if (NULL == *city)
                return false;
        }

        (*city)[length_of_name - 1] = line[i];
    }

    length_of_name++;

    if (length_of_name > size_of_name){
        size_of_name = 2 * size_of_name;
        *city = realloc(*city, size_of_name * sizeof(char));
    }

    (*city)[length_of_name - 1] = '\0';

    return true;
}
/**
 * Funkcja sprawdza, czy w podanym stringu od podanej pozycji do końca napisu lub do śrenika
 * znajduje się poprawny rok i jesli tak, to go wczytuje.
 * @param line - wczytana linia ze standartowego wejścia
 * @param position_of_city - pozycja w napisie od którego zaczyna się rok
 * @param year - wskaźnik na int
 * @return true, jeśli rok jest poprawny i false w przeciwnym wypadku.
 */
static bool getYear( const char *line, int position_of_year, int *year){

    bool is_number_positive = true;
    long long candidate_for_year = 0;

    if (line[position_of_year] == '-'){
        is_number_positive = false;
        position_of_year++;
    }

    for (int i = position_of_year; line[i] != '\0' && line[i] != ';'; ++i){
        if (line[i] < '0' || line[i] > '9')
            return false;

        candidate_for_year = 10 * candidate_for_year + (line[i] - '0');

        if ((true == is_number_positive && candidate_for_year > INT_MAX) ||
        (false == is_number_positive && -candidate_for_year < INT_MIN))
            return false;
    }

    if (false == is_number_positive)
        candidate_for_year = -candidate_for_year;

    if (candidate_for_year == 0)
        return false;

    *year = (int) candidate_for_year;

    return true;
}
/**
 * Funkcja sprawdza, czy w podanym stringu od podanej pozycji do końca napisu lub do śrenika
 * znajduje się poprawna długośc i jesli tak, to ją wczytuje.
 * @param line - wczytana linia ze standartowego wejścia
 * @param position_of_city - pozycja w napisie od którego zaczyna się długość
 * @param length - wskaźnik na int
 * @return true, jeśli długość jest poprawna i false w przeciwnym wypadku.
 */
static bool getLength( const char *line, int position_of_length, unsigned *length) {

    long long candidate_for_length = 0;

    for (int i = position_of_length; line[i] != ';' && line[i] != '\0'; ++i) {
        if (line[i] < '0' || line[i] > '9')
            return false;

        candidate_for_length = 10 * candidate_for_length + (line[i] - '0');

        if (candidate_for_length > UINT_MAX)
            return false;
    }

    if (candidate_for_length == 0)
        return false;

    *length = (int) candidate_for_length;

    return true;
}
/**
 * Funkcja sprawdza, czy w podanym stringu od podanej pozycji do średnika lub do końca napisu
 * znajduje się poprawny numer drogi krajowej i jesli tak, to go wczytuje.
 * @param line - wczytana linia ze standartowego wejścia
 * @param position_of_city - pozycja w napisie od którego zaczyna się nummer drogi krajowej
 * @param routeId  - wskażnik na int
 * @return true, jeśli numer jest poprawny i false w przeciwnym wypadku.
 */
static bool getRouteId( const char *line, int position_of_routeId, unsigned *routeId){

    unsigned candidate_for_routeId = 0;

    for (int i = position_of_routeId; line[i] != ';' && line[i] != '\0'; ++i){
        if (line[i] < '0' || line[i] > '9')
            return false;

        candidate_for_routeId = 10 * candidate_for_routeId + (line[i] - '0');

        if (candidate_for_routeId >= 1000)
            return false;
    }

    if (candidate_for_routeId == 0)
        return false;

    *routeId = candidate_for_routeId;

    return true;
}
/**
 * Funkcja liczy, ile znaków najmuje podana liczba zapisana z użyciem charów.
 * @param number - sprawdzana liczba
 * @return ilość znaków potrzebna do zapisania podanej liczby.
 */
static int lengthOfNumber(long long number){

    int length = 1;

    if (number < 0)
        length++;

    while ((number = number/10) != 0)
        length++;

    return length;
}

bool isItAddRoad( const char *line, char **city1, char **city2, unsigned *length, int *built_year){

    int position_of_first_char_of_city1 = strlen( "addRoad;");
    int position_of_first_char_of_city2;
    int position_of_length;
    int position_of_built_year;

    if( strncmp( line, "addRoad;", position_of_first_char_of_city1) != 0)
        return false;

    if (false == getCityName( line, position_of_first_char_of_city1, city1))
        return false;

    position_of_first_char_of_city2 = position_of_first_char_of_city1 + (int) strlen(*city1) + 1;

    if (line[position_of_first_char_of_city2 - 1] != ';')
        return false;

    if (false == getCityName( line, position_of_first_char_of_city2, city2))
        return false;

    if (strcmp(*city1, *city2) == 0)
        return false;

    position_of_length = position_of_first_char_of_city2 + (int) strlen(*city2) + 1;

    if (line[position_of_length - 1] != ';')
        return false;

    if (false == getLength( line, position_of_length, length))
        return false;

    position_of_built_year = position_of_length + lengthOfNumber( *length) + 1;

    if (line[position_of_built_year - 1] != ';')
        return false;

    if (false == getYear( line, position_of_built_year, built_year))
        return false;

    if (line[ position_of_built_year + lengthOfNumber( *built_year)] != '\0')
        return false;

    return true;
}

bool isItRepairRoad( const char *line, char **city1, char **city2, int *repair_year){

    int position_of_first_char_of_city1 = strlen( "repairRoad;");
    int position_of_first_char_of_city2;
    int position_of_repair_year;

    if( strncmp( line, "repairRoad;", position_of_first_char_of_city1) != 0)
        return false;

    if (false == getCityName( line, position_of_first_char_of_city1, city1))
        return false;

    position_of_first_char_of_city2 = position_of_first_char_of_city1 + (int) strlen(*city1) + 1;

    if (line[position_of_first_char_of_city2 - 1] != ';')
        return false;

    if (false == getCityName( line, position_of_first_char_of_city2, city2))
        return false;

    if (strcmp(*city1, *city2) == 0)
        return false;

    position_of_repair_year = position_of_first_char_of_city2 + (int) strlen(*city2) + 1;

    if (line[position_of_repair_year - 1] != ';')
        return false;

    if (false == getYear( line, position_of_repair_year, repair_year))
        return false;

    if (line[ position_of_repair_year + lengthOfNumber( *repair_year)] != '\0')
        return false;

    return true;
}

bool isItGetRouteDescription( const char *line, unsigned *routeId){

    int position_of_routeId = strlen( "getRouteDescription;");

    if (strncmp( line, "getRouteDescription;", position_of_routeId) != 0)
        return false;

    if (false == getRouteId( line, position_of_routeId, routeId))
        return false;

    if ( line[position_of_routeId + lengthOfNumber( *routeId)] != '\0')
        return false;

    return true;
}

bool isItNewRouteByGivenCities( const char *line, unsigned *routeId, char **city, char ***cities_on_the_route,
        unsigned **lengths_of_route_sections, int **years_of_route_sections, int *length_of_arrays){

    int position_of_city;
    int position_of_length;
    int position_of_built_year;

    int size_of_arrays = 2;

    *cities_on_the_route = realloc(*cities_on_the_route, size_of_arrays * sizeof(char *));
    *lengths_of_route_sections = realloc(*lengths_of_route_sections, size_of_arrays * sizeof(unsigned));
    *years_of_route_sections = realloc(*years_of_route_sections, size_of_arrays * sizeof(int));

    if (*cities_on_the_route == NULL || *lengths_of_route_sections == NULL || *years_of_route_sections == NULL)
        return false;

    if (false == getRouteId( line, 0, routeId))
        return false;

    position_of_city = lengthOfNumber( *routeId) + 1;

    if (line[position_of_city - 1] != ';')
        return false;

    if (false == getCityName( line, position_of_city, city))
        return false;

    position_of_length = position_of_city + (int) strlen(*city) + 1;

    if (line[ position_of_length - 1] != ';')
        return false;

    while (true){
        (*length_of_arrays)++;

        if (*length_of_arrays > size_of_arrays){
            size_of_arrays = 2 * size_of_arrays;

            *cities_on_the_route = realloc(*cities_on_the_route, size_of_arrays * sizeof(char *));
            *lengths_of_route_sections = realloc(*lengths_of_route_sections, size_of_arrays * sizeof(unsigned));
            *years_of_route_sections = realloc(*years_of_route_sections, size_of_arrays * sizeof(int));

            if (*cities_on_the_route == NULL || *lengths_of_route_sections == NULL ||
                *years_of_route_sections == NULL){
                (*length_of_arrays)--;
                return false;
            }
        }

        (*cities_on_the_route)[*length_of_arrays - 1] = malloc(sizeof(char));

        if (NULL == (*cities_on_the_route)[*length_of_arrays - 1])
            return false;

        if (false == getLength( line, position_of_length, *lengths_of_route_sections + *length_of_arrays - 1))
            return false;

        position_of_built_year = position_of_length +
                lengthOfNumber((*lengths_of_route_sections)[*length_of_arrays - 1]) + 1;

        if (line[ position_of_built_year - 1] != ';')
            return false;

        if (false == getYear( line, position_of_built_year, *years_of_route_sections + *length_of_arrays - 1))
            return false;

        position_of_city = position_of_built_year +
                lengthOfNumber((*years_of_route_sections)[*length_of_arrays - 1]) + 1;

        if (line[ position_of_city - 1] != ';')
            return false;

        if (false == getCityName( line, position_of_city, *cities_on_the_route + *length_of_arrays - 1))
            return false;

        position_of_length = position_of_city + (int) strlen((*cities_on_the_route)[*length_of_arrays - 1]) + 1;

        if (line[ position_of_length - 1] == '\0')
            break;
        if (line[ position_of_length - 1] != ';')
            return false;
    }

    return true;
}

bool isItNewRoute( const char *line, unsigned *routeId, char **city1, char **city2){

    int position_of_routeId = strlen( "newRoute;");
    int position_of_first_char_of_city1;
    int position_of_first_char_of_city2;

    if (strncmp( line, "newRoute;", position_of_routeId) != 0)
        return false;

    if (false == getRouteId( line, position_of_routeId, routeId))
        return false;

    position_of_first_char_of_city1 = position_of_routeId + lengthOfNumber( *routeId) + 1;

    if (line[position_of_first_char_of_city1 - 1] != ';')
        return false;

    if (false == getCityName( line, position_of_first_char_of_city1, city1))
        return false;

    position_of_first_char_of_city2 = position_of_first_char_of_city1 + (int) strlen(*city1) + 1;

    if (line[position_of_first_char_of_city2 - 1] != ';')
        return false;

    if (false == getCityName( line, position_of_first_char_of_city2, city2))
        return false;

    if (line[position_of_first_char_of_city2 + strlen(*city2)] != '\0')
        return false;

    return true;
}

bool isItExtendRoute( const char *line, unsigned *routeId, char **city){

    int position_of_routeId = strlen( "extendRoute;");
    int position_of_first_char_of_city;

    if (strncmp( line, "extendRoute;", position_of_routeId) != 0)
        return false;

    if (false == getRouteId( line, position_of_routeId, routeId))
        return false;

    position_of_first_char_of_city = position_of_routeId + lengthOfNumber( *routeId) + 1;

    if (line[position_of_first_char_of_city - 1] != ';')
        return false;

    if (false == getCityName( line, position_of_first_char_of_city, city))
        return false;

    if (line[position_of_first_char_of_city + strlen(*city)] != '\0')
        return false;

    return true;
}

bool isItRemoveRoad( const char *line, char **city1, char **city2){

    int position_of_first_char_of_city1 = strlen( "removeRoad;");
    int position_of_first_char_of_city2;

    if( strncmp( line, "removeRoad;", position_of_first_char_of_city1) != 0)
        return false;

    if (false == getCityName( line, position_of_first_char_of_city1, city1))
        return false;

    position_of_first_char_of_city2 = position_of_first_char_of_city1 + (int) strlen(*city1) + 1;

    if (line[position_of_first_char_of_city2 - 1] != ';')
        return false;

    if (false == getCityName( line, position_of_first_char_of_city2, city2))
        return false;

    if (line[position_of_first_char_of_city2 + strlen(*city2)] != '\0')
        return false;

    return true;
}

bool isItRemoveRoute( const char *line, unsigned *routeId){

    int position_of_routeId = strlen( "removeRoute;");

    if (strncmp( line, "removeRoute;", position_of_routeId) != 0)
        return false;

    if (false == getRouteId( line, position_of_routeId, routeId))
        return false;

    if ( line[position_of_routeId + lengthOfNumber( *routeId)] != '\0')
        return false;

    return true;
}