#include "map.h"
#include "input.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Funkcja zwalnia podaną ilość tablic z tablicy tablic
 * @param cities_on_the_route - wskaźnik na tablicę tablic
 * @param length - ilość zwalnianych miast
 */
static void freeArrays( char **cities_on_the_route, int length){

    if (cities_on_the_route != NULL)
        for (int i = 0; i < length; ++i)
            free(cities_on_the_route[i]);
}
/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę dodawania drogi
 * i jeśli tak, to dodaje ją do struktury.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg
 * @param line - wczytana linia ze standartowego wejścia
 * @return true, jeśli dodana została droga o podanych parametrach i false w przeciwnym wypadku.
 */
static bool tryAddRoad( Map *map, const char *line){

    char *city1 = malloc(0 * sizeof(char));
    char *city2 = malloc(0 * sizeof(char));

    unsigned length;
    int built_year;

    if (true == isItAddRoad( line, &city1, &city2, &length, &built_year))
        if (true == addRoad(map, city1, city2, length, built_year)) {
            free(city1);
            free(city2);
            return true;
        }

    free( city1);
    free( city2);
    return false;
}
/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę naprawiania drogi i czy podana
 * droga istnieje i jeśli tak, to zmienia datę ostatniego remontu podanej drogi.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg
 * @param line - wczytana linia ze standartowego wejścia
 * @return true, jeśli jeśli została zmieniona data ostatniego remontu podanej drogi
 * i false w przeciwnym wypadku.
 */
static bool tryRepairRoad( Map *map, const char *line){

    char *city1 = malloc(0 * sizeof(char));
    char *city2 = malloc(0 * sizeof(char));

    int repair_year;

    if (true == isItRepairRoad( line, &city1, &city2, &repair_year))
        if (true == repairRoad( map, city1, city2, repair_year)){
            free( city1);
            free( city2);
            return true;
        }

    free( city1);
    free( city2);
    return false;
}
/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę dodawania drogi krajowej
 * przez podane miasta i jeśli tak, to dodaje ją do struktury.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg
 * @param line - wczytana linia ze standartowego wejścia
 * @return true, jeśli dodana została droga krajowa o podanych parametrach i false w przeciwnym wypadku.
 */
static bool tryNewRouteByGivenCities( Map *map, const char *line){

    char *city = malloc(0 * sizeof(char));
    char **cities_on_the_route = malloc(0 * sizeof(char *));
    unsigned *lengths_of_route_sections = malloc(0 * sizeof(int));
    int *years_of_route_sections = malloc(0 * sizeof(int));

    unsigned routeId;
    int length_of_arrays = 0;

    if (true == isItNewRouteByGivenCities( line, &routeId, &city, &cities_on_the_route, &lengths_of_route_sections,
                &years_of_route_sections, &length_of_arrays))
        if (true == newRouteByGivenCities(map, routeId, city, cities_on_the_route, lengths_of_route_sections,
                                          years_of_route_sections, length_of_arrays)){
            free(city);
            freeArrays(cities_on_the_route, length_of_arrays);
            free(cities_on_the_route);
            free(lengths_of_route_sections);
            free(years_of_route_sections);
            return true;
        }

    free( city);
    freeArrays( cities_on_the_route, length_of_arrays);
    free( cities_on_the_route);
    free( lengths_of_route_sections);
    free( years_of_route_sections);

    return false;
}
/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę dodawania drogi krajowej
 * i jeśli tak, to dodaje ją do struktury.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg
 * @param line - wczytana linia ze standartowego wejścia
 * @return true jeśli, dodana została droga krajowa o podanych parametrach i false w przeciwnym wypadku.
 */
static bool tryNewRoute( Map *map, const char *line){

    char *city1 = malloc(0 * sizeof(char));
    char *city2 = malloc(0 * sizeof(char));

    unsigned routeId;

    if (true == isItNewRoute( line, &routeId, &city1, &city2))
        if (true == newRoute( map, routeId, city1, city2)) {
            free( city1);
            free( city2);
            return true;
        }

    free( city1);
    free( city2);
    return false;

}
/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę wydłużania drogi krajowej
 * i jeśli tak, to wykonuje tą komendę.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg
 * @param line - wczytana linia ze standartowego wejścia
 * @return true, jeśli droga krajowa została wydłużona do podanego miasta i false w przeciwnym wypadku.
 */
static bool tryExtendRoute( Map *map, const char *line){

    char *city = malloc(0 * sizeof(char));

    unsigned routeId;

    if (true == isItExtendRoute( line, &routeId, &city))
        if (true == extendRoute( map, routeId, city)){
            free( city);
            return true;
        }

    free( city);
    return false;
}
/**
 *
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę usuwnia drogi i jeśli tak, to ją wykonuje.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg
 * @param line - wczytana linia ze standartowego wejścia
 * @return true, jeśli została usunięta podana droga i drogi krajowe przechodzące przez nią zostały poprawnie
 * uzupełnione i false w przeciwnym wypadku.
 */
static bool tryRemoveRoad( Map *map, const char *line){

    char *city1 = malloc(0 * sizeof(char));
    char *city2 = malloc(0 * sizeof(char));

    if (true == isItRemoveRoad( line, &city1, &city2))
        if (true == removeRoad( map, city1, city2)){
            free( city1);
            free( city2);
            return true;
        }

    free( city1);
    free( city2);
    return false;
}
/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę usuwnia drogi krajowej i jeśli tak to, ją wykonuje.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg
 * @param line - wczytana linia ze standartowego wejścia
 * @return true, jeśli została usunięta podana droga krajowa krajowa i false w przeciwnym wypadku.
 */
static bool tryRemoveRoute( Map *map, const char *line){

    unsigned routeId;

    if (true == isItRemoveRoute( line, &routeId))
        if (true == removeRoute( map, routeId))
            return true;

    return false;
}
/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę dostawania opisu drogi krajowej
 * oraz czy istnieje droga krajowa o podanym numerze i jeśli tak, to wypisuje jej opis na standartowe wyjście.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg
 * @param line - wczytana linia ze standartowego wejścia
 * @return true, jeśli isnieje droga krajowa o podanych parametrach oraz udało się dostać jej opis
 * i false w przeciwnym wypadku.
 */
static bool tryGetRouteDescription( Map *map, const char *line){

    unsigned routeId;
    const char *description;

    if (true == isItGetRouteDescription( line, &routeId))
        if (NULL != (description = getRouteDescription(map, routeId))) {
            printf("%s\n", description);
            free((void *) description);
            return true;
        }


    return false;
}
/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę i jeśli tak, to ją wykonuje.
 * @param map – wskaźnik na strukturę przechowującą mapę dróg.
 * @param line - wczytana linia ze standartowego wejścia
 * @return true, jeśli linia zawiera poprawną komendę i jej wywołanie zakończyło się wynikiem
 * pozytywnym i false w przeciwnym razie.
 */
static bool doCommand( Map *map, const char *line){

    if (false == tryAddRoad( map, line) && false == tryRepairRoad( map, line) &&
            false == tryNewRouteByGivenCities( map, line) && false == tryGetRouteDescription( map, line) &&
            false == tryNewRoute( map, line) && false == tryExtendRoute( map, line) &&
            false == tryRemoveRoad( map, line) && false == tryRemoveRoute( map, line))
        return false;

    return true;
}

int main(){

    Map *map = newMap();
    if (map == NULL)
        return 0;

    int line_number = 0;
    char *line;

    while (true){
        line_number++;
        line = getLine();

        if (NULL == line)
            break;

        if (strlen(line) > 0 && EOF == line[strlen(line) - 1]){
            if ('#' != line[0] && EOF != line[0])
                fprintf(stderr, "ERROR %i\n", line_number);

            free( line);
            break;
        }

        if ('#' == line[0] || '\0' == line[0]){
            free( line);
            continue;
        }

        if (false == doCommand( map, line))
            fprintf(stderr, "ERROR %i\n", line_number);

        free( line);
    }

    deleteMap( map);
    return 0;
}
