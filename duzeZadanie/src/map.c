#include "map.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>

/**
 * @brief Funkcja zwraca mniejszą z dwóch liczb.
 * @param a - pierwsza liczba
 * @param b - druga liczba
 * @return Najstarszy odcinek drogi.
 */
static int min( int a, int b){

    return a < b ? a : b;
}
/**
 * @brief Funkcja sprawdza czy dwa napisy są identyczne.
 * Jest to wersja strcmp dla qsort, gdzie podane paramentry są typu const void* i należy je
 * skastować na const char*, aby nadawały się do funkcji strcmp.
 * @param str1 - wskaźnik na pierwszy napis
 * @param str2 - wskaźnik na drugi napis
 * @return 0, jeśli napisy są takie same i inną wartość w przeciwnym wypadku.
 */
int cmpstr(const void* str1, const void* str2)
{
    const char* string1 = *(const char**) str1;
    const char* string2 = *(const char**) str2;
    return strcmp( string1,string2);
}
/**
 * @brief Funkcja liczy pozycję miasta o podanej nazwie.
 * @param key - nazwa miasta
 * @param array_of_cities - tablica miast uporządkowana alfabetycznie
 * @param number_of_cities - liczba miast w tablicy
 * @return Położenie miasta w tablicy lub, jeśli miasta nie ma w tablicy, miejsce w którym mogłoby się znajdować.
 */
static int binSearchForArrayOfCities( const char *key, City **array_of_cities, int number_of_cities){

    if (0 == number_of_cities)
        return 0;

    int l,r,mid;
    l = 0;
    r = number_of_cities-1;
    mid = 0;

    while (l<=r){
        mid = (l+r)/2;

        if (strcmp( key, array_of_cities[mid]->name) < 0)
            r = mid-1;
        else if (strcmp( key, array_of_cities[mid]->name) > 0)
            l = mid+1;
        else
            return mid;
    }

    if (strcmp( key, array_of_cities[mid]->name) < 0)
        return mid;
    else
        return mid+1;
}
/**
 * Funkcja liczy pozycję miasta o podanej nazwie.
 * @param key - nazwa miasta
 * @param array_of_roads -tablica dróg wychodzących z danego miasta uporządkowana alfabetycznie w stosunku do
 * miast do których prowadzi
 * @param number_of_roads - liczba wychodzących dróg
 * @return Położenie miasta w tablicy lub, jeśli miasta nie ma w tablicy, miejsce w którym mogłoby się znajdować.
 */
static int binSearchForArrayOfOutgoingRoads( const char *key, Road ** array_of_roads, int number_of_roads){

    if (0 == number_of_roads)
        return 0;

    int l,r,mid;
    l = 0;
    r = number_of_roads-1;
    mid = 0;

    while (l<=r){
        mid = (l+r)/2;

        if (strcmp( key, array_of_roads[mid]->to_which_city->name) < 0)
            r = mid-1;
        else if (strcmp( key, array_of_roads[mid]->to_which_city->name) > 0)
            l = mid+1;
        else
            return mid;
    }

    if (strcmp( key, array_of_roads[mid]->to_which_city->name) < 0)
        return mid;
    else
        return mid+1;
}
/**
 * @brief Zwraca wskażnik na miasto o podanej nazwie.
 * @param map - wskaźnik na strukturę przechowującą mapę dróg
 * @param name - nazwa miasta
 * @return Wskażnik na miasto lub NULL jeśli miasta nie ma w strukturze.
 */
static City *findCityWithTheGivenName( Map *map, const char *name){

    int position_of_candidate_for_the_searching_city =
            binSearchForArrayOfCities( name, map->array_of_cities, map->number_of_cities);

    if( position_of_candidate_for_the_searching_city < map->number_of_cities &&
    strcmp( name, map->array_of_cities[ position_of_candidate_for_the_searching_city]->name) == 0)
        return map->array_of_cities[ position_of_candidate_for_the_searching_city];
    else
        return NULL;

}
/**
 * @brief Zwraca wskażnik na drogę prowadzącą do danego miasta
 * @param array_of_roads - tablica dróg wychodzących z jednego miasta
 * @param number_of_roads - liczba dróg w tablicy
 * @param name - nazwa miasta
 * @return Wskażnik na drogę lub NULL jeśli miasta nie ma w strukturze.
 */
static Road *findRoadToTheGivenCity( Road ** array_of_roads, int number_of_roads, const char *name){

    int position_of_candidate_for_the_searching_road =
            binSearchForArrayOfOutgoingRoads( name, array_of_roads, number_of_roads);

    if( position_of_candidate_for_the_searching_road < number_of_roads &&
    strcmp( name, array_of_roads[ position_of_candidate_for_the_searching_road]->to_which_city->name) == 0)
        return array_of_roads[ position_of_candidate_for_the_searching_road];
    else
        return NULL;
}
/**
 * @brief Wyszukuje drogi krajowej z podanego miasta do końcowego miasta.
 * @param city - aktualne miasto
 * @param finishing_city - poszukiwane miasto
 * @param distances - tablica z dystansami do danego miasta
 * @param predecessors - tablica z poprzednikami w drofze danego miasta
 * @param years - najstarszy odcinek drogi prowadzący do danego miasta
 * @return Zwraca 0 jeśli znalazł drogę, 2 jeśli do szukanego miasta nie da się jednoznacznie poprowadzić drogi
 * i 1 w przeciwnym wypadku.
 */
static int findRouteToTheGivenCity( City *city, City *finishing_city, unsigned int *distances, int *predecessors, int *years){

    if (city == finishing_city)
        return 0;

    int control_f = 1;
    int control_h;
    Road *road;
    int min_year;

    for (int i = 0; i < city->number_of_outgoing_roads; ++i){
        road = city->array_of_outgoing_roads[i];
        min_year = min( road->construction_or_repair_year, years[ city->position_on_map]);

        if (distances[ city->position_on_map] + road->length < distances[ road->to_which_city->position_on_map]){

            years[ road->to_which_city->position_on_map] = min_year;
            distances[ road->to_which_city->position_on_map] = distances[ city->position_on_map] + road->length;
            predecessors[ road->to_which_city->position_on_map] = city->position_on_map;

            control_h = findRouteToTheGivenCity( road->to_which_city, finishing_city, distances, predecessors, years);
            if (control_h != 1)
                control_f = control_h;
        }

        else if (distances[ city->position_on_map] + road->length == distances[ road->to_which_city->position_on_map]){

            if (min_year == years[ road->to_which_city->position_on_map] && road->to_which_city == finishing_city)
                control_f = 2;

            else if (min_year >= years[ road->to_which_city->position_on_map]){

                years[ road->to_which_city->position_on_map] = min_year;
                distances[ road->to_which_city->position_on_map] = distances[ city->position_on_map] + road->length;
                predecessors[ road->to_which_city->position_on_map] = city->position_on_map;

                control_h = findRouteToTheGivenCity( road->to_which_city, finishing_city, distances, predecessors, years);
                if (control_h != 1)
                    control_f = control_h;
            }
        }
    }

    return control_f;
}
/**
 * @brief Wyszukuje drogę z starting_city do finishing city.
 * @param map - wskażnik na strukturę zwierającą mapę
 * @param starting_city - miasto z którego szukamy drogi
 * @param finishing_city - miasto do którego szukamy drogi
 * @param distances - tablica z dystansami do danego miasta
 * @param predecessors - tablica z poprzednikami w drodze danego miasta
 * @param years - najstarszy odcinek drogi prowadzący do danego miasta
 * @return Zwraca true jeśli wyznaczył drogę i false w przeciwnym wypadku.
 */
static bool findRoute( Map *map, City *starting_city, City *finishing_city, unsigned int *distances, int *predecessors,
        int *years){

    if (NULL == distances || NULL == predecessors || NULL == years || starting_city == finishing_city)
        return false;

    for (int i = 0; i < map->number_of_cities; ++i){
        predecessors[i] = finishing_city->position_on_map;
        years[i] = INT_MAX;
    }
    distances[ starting_city->position_on_map] = 0;
    distances[ finishing_city->position_on_map] = UINT_MAX;

    if (0 != findRouteToTheGivenCity( starting_city, finishing_city, distances, predecessors, years))
        return false;

    return true;
}
/**
 * @brief Dla każdego odcinka drogi krajowej route dodaje do jego tablicy dróg krajowych tą drogę.
 * @param route_sections - tablica odcinków składowych drogi krajowej
 * @param length_of_array - długość tablicy odcinków
 * @param routeID - numer drogi krajowej
 * @return true, jeśli dodało drogę dla każdego odcinka i false w przeciwnym wypadku.
 */
static bool addRouteToArraysOfRoutesPassingThroughThisRoad( Road **route_sections, int length_of_array, unsigned routeID){

    Road *road1;
    Road *road2;

    for (int i = 0; i < length_of_array; ++i){
        road1 = route_sections[i];

        if (road1->length_of_array_of_passing_route == road1->number_of_routes_passing_through_this_road){

            road1->length_of_array_of_passing_route++;
            road1->array_of_routes_passing_through_this_road = realloc(road1->array_of_routes_passing_through_this_road,
                    road1->length_of_array_of_passing_route * sizeof(int*));

            if (road1->array_of_routes_passing_through_this_road == NULL)
                return false;
        }

        road2 = findRoadToTheGivenCity( road1->to_which_city->array_of_outgoing_roads,
                road1->to_which_city->number_of_outgoing_roads, road1->from_which_city->name);

        if (road2->length_of_array_of_passing_route == road2->number_of_routes_passing_through_this_road){

            road2->length_of_array_of_passing_route++;
            road2->array_of_routes_passing_through_this_road = realloc(road2->array_of_routes_passing_through_this_road,
                    road2->length_of_array_of_passing_route * sizeof(int*));

            if (road2->array_of_routes_passing_through_this_road == NULL)
                return false;
        }
    }

    for (int i = 0; i < length_of_array; ++i){
        road1 = route_sections[i];
        road1->array_of_routes_passing_through_this_road[ road1->number_of_routes_passing_through_this_road] = routeID;
        road1->number_of_routes_passing_through_this_road++;

        road2 = findRoadToTheGivenCity( road1->to_which_city->array_of_outgoing_roads,
                road1->to_which_city->number_of_outgoing_roads, road1->from_which_city->name);
        road2->array_of_routes_passing_through_this_road[ road2->number_of_routes_passing_through_this_road] = routeID;
        road2->number_of_routes_passing_through_this_road++;
    }

    return true;
}
/**
 * @brief zwraca drogę krajową z starting_city do finishing city.
 * @param map - wskażnik na strukturę zwierającą mapę
 * @param starting_city - miasto z którego szukamy drogi
 * @param finishing_city - miasto do którego szukamy drogi
 * @param predecessors - tablica z poprzednikami w drodze danego miasta
 * @param length1 - ilość odcinków z których składa się droga krajowa
 * @param routeID - numer drogi krajowej do której mają należeć zwracane odcinki
 * @return wskażnik, na drogę krajową lub NULL jeśli nie udało się zaalokować pamięci.
 */
static Road **returnRouteSections( Map *map, City *starting_city, City *finishing_city, const int *predecessors,
                            int *length1, unsigned int routeID){

    int length = 0;
    Road **route_sections;

    for (int i = finishing_city->position_on_map; i != starting_city->position_on_map; i = predecessors[i]){
        length++;
    }

    route_sections = malloc(length * sizeof(Road*));
    if (route_sections == NULL)
        return NULL;

    for (int i = finishing_city->position_on_map, k = length-1;
         i != starting_city->position_on_map; i = predecessors[i], k--){
        route_sections[k] = findRoadToTheGivenCity(
                map->array_of_cities[ predecessors[i]]->array_of_outgoing_roads,
                map->array_of_cities[ predecessors[i]]->number_of_outgoing_roads, map->array_of_cities[i]->name);
    }

    if (addRouteToArraysOfRoutesPassingThroughThisRoad( route_sections, length, routeID) == false){
        free(route_sections);
        return NULL;
    }

    *length1 = length;

    return route_sections;
}
/**
 * @brief Funkcja wydłuża drogę krajową na podstawie tablicy predecessors i dodaje wydłużenie na początek.
 * @param map - wskażnik na strukturę zwierającą mapę
 * @param route - wskażnik na drogę do której chcemy dołączyć wydłużenie
 * @param city - miasto do którego wydłużamy droge
 * @param starting_city - miasto początkowe drogi krajowej
 * @param predecessors - tablica z poprzednikami w drodze danego miasta
 * @param routeID - numer wydłużanej drogi
 * @return true, jeśli udało się wydłuzyć drogę i false w przeciwnym wypadku.
 */
static bool addExtensionAtTheBeginning( Map *map, Route *route, City *city, City *starting_city, int *predecessors,
        unsigned routeID){

    int length;
    Road **array_of_roads = returnRouteSections( map, city, starting_city, predecessors, &length, routeID);

    if (NULL == array_of_roads)
        return false;

    route->array_of_route_sections = realloc(route->array_of_route_sections,
            (route->number_of_route_sections + length) * sizeof(Road*));

    if (NULL == route->array_of_route_sections){
        free( array_of_roads);
        return false;
    }

    for (int i = route->number_of_route_sections-1; i >= 0; --i)
        route->array_of_route_sections[i + length] = route->array_of_route_sections[i];

    for (int i = 0; i < length; ++i)
        route->array_of_route_sections[i] = array_of_roads[i];

    route->number_of_route_sections += length;
    route->from_which_city = city;

    free( array_of_roads);

    return true;
}
/**
 * @brief Funkcja wydłuża drogę krajową na podstawie tablicy predecessors i dodaje wydłużenie na koniec.
 * @param map - wskażnik na strukturę zwierającą mapę
 * @param route - wskażnik na drogę do której chcemy dołączyć wydłużenie
 * @param city - miasto do którego wydłużamy droge
 * @param finishing_city - miasto końcowe drogi
 * @param predecessors - tablica z poprzednikami w drodze danego miasta
 * @param routeID - numer wydłużanej drogi
 * @return Zwraca true jeśli udało się wydłuzyć drogę i false w przeciwnym wypadku.
 */
static bool addExtensionAtTheEnd( Map *map, Route *route, City *city, City *finishing_city, int *predecessors,
        unsigned routeID){

    int length;
    Road **array_of_roads = returnRouteSections( map, finishing_city, city, predecessors, &length, routeID);

    if (NULL == array_of_roads)
        return false;

    route->array_of_route_sections = realloc(route->array_of_route_sections,
            (route->number_of_route_sections + length) * sizeof(Road*));

    if (NULL == route->array_of_route_sections){
        free( array_of_roads);
        return false;
    }

    for (int i = 0; i < length; ++i)
        route->array_of_route_sections[i + route->number_of_route_sections] = array_of_roads[i];

    route->number_of_route_sections += length;
    route->to_which_city = city;

    free( array_of_roads);

    return true;
}
/**
 * @brief Funkcja sprawdza czy krótsze będzie wydłużenie drogi na początku, czy na końcu i wydłuża odpowiednio drogę.
 * @param map - wskażnik na strukturę zwierającą mapę
 * @param route - wskażnik na drogę do której chcemy dołączyć wydłużenie
 * @param city - miasto do którego wydłużamy droge
 * @param starting_city - miasto początkowe drogi
 * @param finishing_city - miasto końcowe drogi
 * @param distances1 - tablica z dystansami do danego miasta
 * @param predecessors1 - tablica z poprzednikami w drofze danego miasta
 * @param years1 - najstarszy odcinek drogi prowadzący do danego miasta
 * @param distances2 - tablica z dystansami do danego miasta
 * @param predecessors2 - tablica z poprzednikami w drofze danego miasta
 * @param years2 - najstarszy odcinek drogi prowadzący do danego miast
 * @param routeID - numer wydłużanej drogi
 * @return true, jeśli udało się wydłuzyć drogę i false w przeciwnym wypadku.
 */
static bool addShorterExtension( Map *map, Route * route, City *city, City *starting_city, City *finishing_city,
        const unsigned int *distances1, int *predecessors1, const int *years1,
        const unsigned int *distances2, int *predecessors2, const int *years2, unsigned routeID){

    int position = city->position_on_map;

    if (distances1[ starting_city->position_on_map] == distances2[ position] &&
            years1[ starting_city->position_on_map] == years2[ position])
        return false;

    if (distances1[ starting_city->position_on_map] == distances2[ position]){
        if( years1[ starting_city->position_on_map] > years2[ position])
            return addExtensionAtTheBeginning( map, route, city, starting_city, predecessors1, routeID);
        else
            return addExtensionAtTheEnd( map, route, city, finishing_city, predecessors2, routeID);
    }

   if (distances1[ starting_city->position_on_map] < distances2[ position])
       return  addExtensionAtTheBeginning( map, route, city, starting_city, predecessors1, routeID);

   else
       return addExtensionAtTheEnd( map, route, city, finishing_city, predecessors2, routeID);
}
/**
 * @brief Funkcja usuwa drogę o podanej pozycji.
 * @param city - wskażnik na miasto z którego droga wychodzi
 * @param position - pozycja usuwanej drogi
 */
static void removeRoadFromArray( City *city, int position){

    for (int i = position; i < city->number_of_outgoing_roads - 1; ++i){
        city->array_of_outgoing_roads[i] = city->array_of_outgoing_roads[i+1];
    }

    city->number_of_outgoing_roads--;
}
/**
 * @brief Funkcja dodaje podaną drogę do tablicy dróg przechodzących przez podane miasto.
 * @param city - nazwa dodawanej drogi
 * @param position - pozycja dodawanej drogi
 * @param road - dodawana droga
 */
static void addRoadToArray( City *city, int position, Road *road){

    for (int i = city->number_of_outgoing_roads; i >position; --i)
        city->array_of_outgoing_roads[i] = city->array_of_outgoing_roads[i-1];

    city->array_of_outgoing_roads[ position] = road;
    city->number_of_outgoing_roads++;
}
/**
 * @brief Funkcja dodaje miasto o podanej nazwie do mapy na podaną pozycję.
 * @param map - wskażnik na strukturę zwierającą mapę
 * @param name - nazwa miasta
 * @param where_add - pozycja dodawanego miasta
 * @return true, jeśli udało się dodać miasto i false w przeciwnym wypadku.
 */
static bool addCity( Map *map, const char *name, int where_add){

    City *new_city = malloc( sizeof(City));
    if (NULL == new_city)
        return false;

    new_city->array_of_outgoing_roads = malloc(2 * sizeof(Road*));
    if (NULL == new_city->array_of_outgoing_roads){
        free( new_city);
        return false;
    }

    new_city->name = malloc((strlen( name) + 1) * sizeof(char));
    if (NULL == new_city->name){
        free( new_city->array_of_outgoing_roads);
        free( new_city);
        return false;
    }

    strcpy( new_city->name, name);
    new_city->length_of_array_of_outgoing_roads = 2;
    new_city->number_of_outgoing_roads = 0;
    new_city->position_on_map = where_add;

    if (map->number_of_cities == map->length_of_array_of_cities){
        map->length_of_array_of_cities = 2 * map->length_of_array_of_cities;
        map->array_of_cities = realloc( map->array_of_cities,  map->length_of_array_of_cities * sizeof(City*));

        if (NULL == map->array_of_cities){
            free(new_city->name);
            free( new_city->array_of_outgoing_roads);
            free( new_city);
            return false;
        }
    }

    for (int i = map->number_of_cities - 1; i >= where_add; --i){
        map->array_of_cities[i+1] = map->array_of_cities[i];
        map->array_of_cities[i+1]->position_on_map++;
    }

    map->array_of_cities[ where_add] = new_city;
    map->number_of_cities++;

    return true;
}
/**
 * @brief Funkcja dodaje drogę o podanych parametrach do mapy na podaną pozycję.
 * @param map - wskażnik na strukturę zwierającą mapę
 * @param routeId - numer drogi
 * @param starting_city - miasto startowe drogi
 * @param finishing_city - miasto końcowe drogi
 * @param predecessors - tablica z poprzednikami w drodze danych miast
 * @return true, jeśli udało się dodać drogę i false w przeciwnym wypadku
 */
static bool addRoute( Map *map, unsigned routeId, City *starting_city, City *finishing_city, const int *predecessors){

    Route *route = malloc( sizeof(Route));
    if (NULL == route)
        return false;

    route->from_which_city = starting_city;
    route->to_which_city = finishing_city;
    route->number_of_route_sections = 0;

    for (int i = finishing_city->position_on_map; i != starting_city->position_on_map; i = predecessors[i])
        route->number_of_route_sections++;

    int length;
    route->array_of_route_sections = returnRouteSections( map, starting_city, finishing_city, predecessors, &length,
            routeId);
    if (NULL == route->array_of_route_sections){
        free( route);
        return false;
    }

    map->array_of_routes[ routeId] = route;

    return true;
}
/**
 * @brief Funkcja dodaje drogę z jednego miasta do drugiego.
 * @param city1 - skąd
 * @param city2 - dokąd
 * @param length - długość drogi
 * @param built_year - rok budowy lub ostatniego remonu drogi
 * @return true, jeśli udało się dodać drogę i false w przeciwnym wypadku
 */
static bool addRoadFromTheGivenCityToAnotherGivenCity( City *city1, City *city2, unsigned int length, int built_year){

    int position_of_the_added_road;
    Road *new_road = malloc( sizeof(Road));

    if (NULL == new_road)
        return false;

    new_road->from_which_city = city1;
    new_road->to_which_city = city2;
    new_road->length = length;
    new_road->construction_or_repair_year = built_year;
    new_road->array_of_routes_passing_through_this_road = malloc(0 * sizeof(int*));
    new_road->length_of_array_of_passing_route = 0;
    new_road->number_of_routes_passing_through_this_road = 0;

    position_of_the_added_road = binSearchForArrayOfOutgoingRoads( city2->name,
            city1->array_of_outgoing_roads, city1->number_of_outgoing_roads);

    if (city1->number_of_outgoing_roads == city1->length_of_array_of_outgoing_roads){
        city1->length_of_array_of_outgoing_roads = 2 * city1->length_of_array_of_outgoing_roads;
        city1->array_of_outgoing_roads = realloc( city1->array_of_outgoing_roads,
                                                  city1->length_of_array_of_outgoing_roads * sizeof(Road*));

        if (NULL == city1->array_of_outgoing_roads){
            free( new_road->array_of_routes_passing_through_this_road);
            free( new_road);
            return false;
        }
    }

    for (int i = city1->number_of_outgoing_roads - 1; i >= position_of_the_added_road; --i)
        city1->array_of_outgoing_roads[i+1] = city1->array_of_outgoing_roads[i];

    city1->array_of_outgoing_roads[ position_of_the_added_road] = new_road;
    city1->number_of_outgoing_roads++;

    return true;
}
/**
 * @brief Funkcja sprawdza czy podana nazwa miasta jest poprawna.
 * @param name - nazwa miasta
 * @return Zwraca true jeśli jest poprawna i false w przeciwnym wypadku.
 */
static bool isTheNameCorrect( const char *name){

    if (name[0] == '\0') return false;

    for (int i = 0; name[i] != '\0' ; ++i)
        if ((name[i] >= 0 && name[i] <= 31) || ';' == name[i])
            return false;

    return true;
}
/**
 * @brief Funkcja sprawdza czy podane miasto leży na trasie podanej drogi.
 * @param city - wskaźnik na sprawdzane miasto
 * @param route - wskaźnik na sprawdzaną drogę
 * @return Zwraca true jeśli miasto należy do drogi i false w przeciwnym wypadku.
 */
static bool doesCityBelongToTheRoad( City *city, Route *route){

    if (route->from_which_city == city)
        return true;

    for (int i = 0; i < route->number_of_route_sections; ++i)
        if (route->array_of_route_sections[i]->to_which_city == city)
            return true;

    return false;
}
/**
 * @brief Funkcja zwiększa dwukrotnie długość stringa.
 * @param string - dany string
 * @param length_of_string - aktualna długość stringa
 * @return Zwraca wskażnik na stringa, jeśli udało się zwiększyć długość i NULL w przeciwnym wypadku.
 */
static char *increaseTheSizeOfTheString( char *string, int *length_of_string){

    *length_of_string = 2 * (*length_of_string);
    string = realloc(string, *length_of_string * sizeof(char));

    return string;
}
/**
 * @brief Dodaje do opisu drogi jej numer i miasto startowe.
 * @param route_description - wskażnik na opis drogi
 * @param routeId - numer drogi
 * @param length_of_allocated_memory_for_route_description - długość stringa
 * @param length_of_route_description - aktualnie wykorzystywana długość stringa
 * @param route - wskażnik na drogę krajową
 * @return Zwraca opis drogi jeśli udało się dodać jej numer i miasto startowe, w przeciwnym wypadku zwraca NULL.
 */
static char *addToDescriptionNumberAndStartingCity( char *route_description, unsigned routeId,
        int *length_of_allocated_memory_for_route_description, int *length_of_route_description, Route *route){

    char *integral = malloc(25 * sizeof(char));
    if (NULL == integral)
        return NULL;

    sprintf( integral, "%i", routeId);
    *length_of_route_description += (int) strlen( integral) + (int) strlen( route->from_which_city->name) + 1;

    while (*length_of_route_description > *length_of_allocated_memory_for_route_description){
        route_description = increaseTheSizeOfTheString( route_description,
                length_of_allocated_memory_for_route_description);

        if (NULL == route_description) {
            free( integral);
            return NULL;
        }
    }

    strcat( route_description, integral);
    strcat( route_description, ";");
    strcat( route_description, route->from_which_city->name);

    free( integral);

    return route_description;
}
/**
 * @brief Dodaje do opisu drogi krajowej długość, rok i miasto końcowe jednej drogi, która wchodzi w jej skład.
 * @param route_description - wskażnik na opis drogi
 * @param length_of_allocated_memory_for_route_description - długość stringa
 * @param length_of_route_description - aktualnie wykorzystywana długość stringa
 * @param road - wskażnik na drogę, której opis chcemy dodać
 * @return Zwraca opis drogi jeśli udało się dodać opis danego fragmenu, w przeciwnym wypadku zwraca NULL.
 */
static char *addDescriptionOfOneRoad( char *route_description, int *length_of_allocated_memory_for_route_description,
                               int *length_of_route_description, Road *road){

    char *integral = malloc(25 * sizeof(char));
    char *string = malloc( (51 + strlen(road->to_which_city->name)) * sizeof(char));
    if (NULL == integral || NULL == string){
        free( integral);
        free( string);
        return NULL;
    }

    sprintf( integral, "%i", road->length);
    *length_of_route_description += (int) strlen( integral) + 1;

    while (*length_of_route_description > *length_of_allocated_memory_for_route_description){
        route_description = increaseTheSizeOfTheString( route_description,
                length_of_allocated_memory_for_route_description);

        if (NULL == route_description){
            free( integral);
            free( string);
            return NULL;
        }
    }

    strcpy(string, ";");
    strcat(string, integral);
    strcat( route_description + *length_of_route_description - (int) strlen( integral) - 2, string);

    sprintf( integral, "%i", road->construction_or_repair_year);
    *length_of_route_description += (int) strlen( integral) + (int) strlen( road->to_which_city->name) + 2;

    while (*length_of_route_description > *length_of_allocated_memory_for_route_description){
        route_description = increaseTheSizeOfTheString( route_description,
                length_of_allocated_memory_for_route_description);

        if (NULL == route_description){
            free( integral);
            free( string);
            return NULL;
        }
    }

    strcpy( string,";");
    strcat( string, integral);
    strcat( string, ";");
    strcat( string, road->to_which_city->name);
    strcat( route_description + *length_of_route_description - (int) strlen( integral) -
        (int) strlen( road->to_which_city->name) - 3, string);

    free( string);
    free( integral);

    return route_description;
}
/**
 * @brief Uzupełnia jedną drogę z usuniętym fragmentem.
 * @param roads - wskażnik na fragmenty dróg, o które uzupełniamy drogę krajową
 * @param length - ilość uzupełnianych fragmentów
 * @param route - wskażnik na uzupełnianą drogę krajową
 * @param city1 - odkąd uzupełniamy drogę
 * @param city2 - dokąd uzupełniamy drogę
 */
static void addRoadsToRoute( Road **roads, int length, Route *route, City *city1, City *city2){

    int position1;
    int position2;

    if (strcmp( route->from_which_city->name, city1->name) == 0)
        position1 = -1;
    else
        for (position1 = 0; position1 < route->number_of_route_sections; ++position1)
            if (route->array_of_route_sections[ position1]->to_which_city == city1)
                break;

    if (strcmp( route->from_which_city->name, city2->name) == 0)
        position2 = -1;
    else
        for (position2 = 0; position2 < route->number_of_route_sections; ++position2)
            if (route->array_of_route_sections[ position2]->to_which_city == city2)
                break;

    if (position1 < position2){
        for (int i = route->number_of_route_sections - 1; i >= position2; --i)
            route->array_of_route_sections[i + length - 1] = route->array_of_route_sections[i];

        for (int i = 0; i < length; ++i)
            route->array_of_route_sections[position2 + i] = roads[i];
    }

    else{
        for (int i = route->number_of_route_sections - 1; i >= position1; --i)
            route->array_of_route_sections[i + length - 1] = route->array_of_route_sections[i];

        for (int i = 0; i < length; ++i) {
            route->array_of_route_sections[position1 + i] = findRoadToTheGivenCity( roads[length - 1 - i]->
                    to_which_city->array_of_outgoing_roads, roads[length - 1 - i]->to_which_city->
                    number_of_outgoing_roads, roads[length - 1 - i]->from_which_city->name);
        }
    }
}
/**
 * @brief Uzupełnia drogę z usuniętym fragmentem.
 * @param map - wskażik na strukturę przechowującą mapę
 * @param road - wskażnik na usuwaną drogę
 * @param city1 - odkąd uzupełniamy drogi
 * @param city2 - dokąd uzupełniamy drogi
 * @param predecessors - tablica z poprzednikami w drodze danych miast
 * @return true, jeśli udało się uzupełnić drogę i false w przeciwnym wypadku.
 */
static bool completeRoutes( Map  *map, City *city1, City *city2, int *predecessors, unsigned routeID){

    int length;
    Road **roads = returnRouteSections( map, city1, city2, predecessors, &length, routeID);
    Route *route;

    if (NULL == roads)
        return false;

    route = map->array_of_routes[ routeID];

    route->array_of_route_sections = realloc(route->array_of_route_sections,
            (route->number_of_route_sections + length - 1) * sizeof( Road*));

    if (NULL == route->array_of_route_sections){
        free( roads);
        return false;
    }

    addRoadsToRoute( roads, length, route, city1, city2);

    route->number_of_route_sections += length -1;
    free( roads);

    return true;
}
/**
 * @brief Funkcja sprawdza czy parametry podane przy tworzeniu nowej drogi przez podane miasta są poprawne.
 * @param map - wskaźnik na strukturę przechowującą mapę dróg;
 * @param city1 - wskażnik na początkowe miasto drogi
 * @param cities_on_the_route - wskażnik na tablice miast znajdujących się po drodze krajowej
 * @param lengths_of_route_sections - wskażnik na tablicę długośći fragmentów drogi krajowej
 * @param years_of_route_sections - wskażnik na tablicę lat fragmentów drogi krajowej
 * @param length_of_arrays - długość wszystkich trzech tablic
 * @return true, jeśli parametry są poprawne i false w przeciwnym wypadku.
 */
static bool areParametersCorrectInFunctionNewRouteByGivenCities( Map *map, const char *city1, char **cities_on_the_route,
        const unsigned *lengths_of_route_sections, const int *years_of_route_sections, int length_of_arrays) {

    if (false == isTheNameCorrect(city1) || 0 == length_of_arrays)
        return false;

    City *my_city1 = findCityWithTheGivenName(map, city1);
    City *my_city2;
    Road *road;

    for (int i = 0; i < length_of_arrays; ++i) {
        if (isTheNameCorrect(cities_on_the_route[i]) == false || years_of_route_sections[i] == 0 ||
            lengths_of_route_sections[i] == 0)
            return false;

        my_city2 = findCityWithTheGivenName(map, cities_on_the_route[i]);

        if (my_city1 != NULL && my_city2 != NULL && (road = findRoadToTheGivenCity(my_city1->array_of_outgoing_roads,
                                                                                   my_city1->number_of_outgoing_roads,
                                                                                   my_city2->name)) != NULL) {

            if (road->length != lengths_of_route_sections[i] ||
                road->construction_or_repair_year > years_of_route_sections[i])
                return false;
        }

        my_city1 = my_city2;
    }

    char **cities = malloc((length_of_arrays + 1) * sizeof(char *));

    if (cities == NULL)
        return false;

    cities[0] = (char*) city1;
    for (int i = 0; i < length_of_arrays; ++i)
        cities[i + 1] = cities_on_the_route[i];

    qsort( cities, length_of_arrays + 1, sizeof(char *), cmpstr);

    for (int i = 0; i < length_of_arrays - 1; ++i)
        if (0 == strcmp(cities[i], cities[i + 1])){
            free(cities);
            return false;
        }

    free(cities);

    return true;
}
/**
 * @brief Zwalnia pamięć przeznaczoną na 3 tablibe tablic o tej samej długości.
 * @param distances - pierwsza tablica tablic
 * @param predecessors - druga tablica tablic
 * @param years - trzecia tablica tablic
 * @param length - długość tablic tablic
 */
static void freeArrays( unsigned **distances, int **predecessors, int **years, int length){

    for (int i = 0; i < length; ++i) {
        free( distances[i]);
        free( predecessors[i]);
        free( years[i]);
    }

    free( distances);
    free( predecessors);
    free( years);
}
/**
 * @brief Alokuje każdą tablicę w trzech tablicach tablic i ustawia dla każdej tablicy w distances i odpowiadającej
 * jej drodze krajowe, na pozycjach odpowiadającym miastom leżących na drodze krajowej, poza pozycją miasta city,
 * wartość 0 i dla pozostałych pozycji ustawia wartość UINT_MAX.
 * @param map - wskaźnik na strukturę przechowującą mapę dróg
 * @param distances - tablica tablic z dystansami do danego miasta
 * @param predecessors - tablica tablic z poprzednikami w drodze danego miasta
 * @param years  - tablica najstarszych odcinków drogi prowadzących do danego miasta
 * @param road - usuwany odcinek drogi
 * @param city - miasto do którego prowadzi usuwany fragment
 * @return true, jeśli udało się zainicjować i ustawić poprawnie tablice i false w przeciwnym wypadku.
 */
static bool setArrays( Map *map, unsigned **distances, int **predecessors, int **years, Road *road, City *city) {

    int length = road->number_of_routes_passing_through_this_road;
    unsigned routeID;
    Route *route;

    for (int i = 0; i < length; ++i){
        distances[i] = malloc( map->number_of_cities * sizeof(int));
        predecessors[i] = malloc( map->number_of_cities * sizeof(int));
        years[i] = malloc( map->number_of_cities * sizeof(int));

        if (NULL == distances[i] || NULL == predecessors[i] || NULL == years[i]){
            freeArrays( distances, predecessors, years, i + 1);
            return false;
        }

        for (int j = 0; j < map->number_of_cities; ++j)
            distances[i][j] = UINT_MAX;
    }

    for (int i = 0; i < length; ++i){
        routeID = road->array_of_routes_passing_through_this_road[i];
        route = map->array_of_routes[ routeID];

        distances[i][ route->from_which_city->position_on_map] = 0;

        for (int j = 0; j < route->number_of_route_sections; ++j)
            distances[i][ route->array_of_route_sections[j]->to_which_city->position_on_map] = 0;

        distances[i][ city->position_on_map] = UINT_MAX;
    }

    return true;
}
/**
 * @brief Funkcja wyszukuje drogę między dwoma miastami dla każdej drogi krajowej, która zawierała wcześniej odcinek
 * pomiędzy tymi miastami.
 * @param map - wskaźnik na strukturę przechowującą mapę dróg
 * @param city1 - wskaźnik na pierwsze miasto
 * @param city2 - wskaźnik na drugie miasto
 * @param distances - tablica tablic z dystansami do danego miasta
 * @param predecessors - tablica tablic z poprzednikami w drodze danego miasta
 * @param years  - tablica najstarszych odcinków drogi prowadzących do danego miasta
 * @param length - długość tablic tablic równa ilości miast w mapie
 * @return true, jeśli udało się znalężć uzupełnienie dla każdej drogi krajowej i false w pzeciwnym wypadku
 */
static bool findRoutes(  Map *map, City *city1, City *city2, unsigned **distances, int **predecessors, int **years,
                     int length){

    for (int i = 0; i < length; ++i)
        if (false == findRoute(map, city1, city2, distances[i], predecessors[i], years[i]))
            return false;

    return true;
}

Map *newMap(){

    Map *map = malloc( sizeof(Map));
    if (NULL == map)
        return NULL;

    map->array_of_cities = malloc(2 * sizeof(City*));
    if (NULL == map->array_of_cities){
        free( map);
        return NULL;
    }

    map->array_of_routes = malloc(1000 * sizeof(Route*));
    if (NULL == map->array_of_routes){
        free( map->array_of_cities);
        free( map);
        return NULL;
    }

    for (int i = 0; i < 1000; ++i)
        map->array_of_routes[i] = NULL;

    map->length_of_array_of_cities = 2;
    map->number_of_cities = 0;

    return map;
}

void deleteMap( Map *map){

    if (NULL == map)
        return;

    for (int i = 0; i < map->number_of_cities ; ++i){
        for (int j = 0; j < map->array_of_cities[i]->number_of_outgoing_roads; ++j){
            free( map->array_of_cities[i]->array_of_outgoing_roads[j]->array_of_routes_passing_through_this_road);
            free( map->array_of_cities[i]->array_of_outgoing_roads[j]);
        }

        free( map->array_of_cities[i]->array_of_outgoing_roads);
        free( map->array_of_cities[i]->name);
        free( map->array_of_cities[i]);
    }
    free( map->array_of_cities);

    for (int i = 0; i <1000; ++i){
        if (NULL != map->array_of_routes[i]){
            free( map->array_of_routes[i]->array_of_route_sections);
            free( map->array_of_routes[i]);
        }
    }
    free( map->array_of_routes);

    free( map);
}

bool addRoad( Map *map, const char *city1, const char *city2, unsigned length, int built_year){

    if (NULL == map)
        return false;

    if (false == isTheNameCorrect( city1) || false == isTheNameCorrect( city2) || 0 == length || 0 == built_year)
        return false;

    int position_of_city1 = binSearchForArrayOfCities( city1, map->array_of_cities, map->number_of_cities);
    if ( position_of_city1 >= map->number_of_cities ||
         strcmp( city1, map->array_of_cities[ position_of_city1]->name) != 0) {

        if (false == addCity(map, city1, position_of_city1))
            return false;
    }

    int position_of_city2 = binSearchForArrayOfCities( city2, map->array_of_cities, map->number_of_cities);
    if ( position_of_city2 >= map->number_of_cities ||
         strcmp( city2, map->array_of_cities[ position_of_city2]->name) != 0){

        if (false == addCity(map, city2, position_of_city2))
            return false;

        if (position_of_city1 >= position_of_city2)
            position_of_city1++;
    }

    if (findRoadToTheGivenCity( map->array_of_cities[ position_of_city1]->array_of_outgoing_roads,
            map->array_of_cities[ position_of_city1]->number_of_outgoing_roads, city2) != NULL)
        return false;

    if (false == addRoadFromTheGivenCityToAnotherGivenCity(map->array_of_cities[position_of_city1],
            map->array_of_cities[position_of_city2], length, built_year))
        return false;

    if (false == addRoadFromTheGivenCityToAnotherGivenCity(map->array_of_cities[position_of_city2],
            map->array_of_cities[position_of_city1], length, built_year))
        return false;

    return true;
}

bool repairRoad( Map *map, const char *city1, const char *city2, int repair_year){

    if (NULL == map || 0 == repair_year)
        return false;

    City *city = findCityWithTheGivenName( map, city1);
    if (NULL == city)
        return false;

    Road *road = findRoadToTheGivenCity( city->array_of_outgoing_roads, city->number_of_outgoing_roads, city2);
    if (NULL == road || repair_year < road->construction_or_repair_year)
        return false;

    road->construction_or_repair_year = repair_year;

    city = findCityWithTheGivenName( map, city2);
    road = findRoadToTheGivenCity( city->array_of_outgoing_roads, city->number_of_outgoing_roads, city1);
    road->construction_or_repair_year = repair_year;

    return true;
}

bool newRoute( Map *map, unsigned routeId, const char *city1, const char *city2){

    if (NULL == map || routeId == 0 || routeId >= 1000 || NULL != map->array_of_routes[routeId])
        return false;

    City *starting_city = findCityWithTheGivenName( map, city1);
    City *finishing_city = findCityWithTheGivenName( map, city2);

    if (NULL == starting_city || NULL == finishing_city || starting_city == finishing_city)
        return false;

    unsigned int *distances = malloc( map->number_of_cities * sizeof(int*));
    int *predecessors = malloc( map->number_of_cities * sizeof(int*));
    int *years = malloc( map->number_of_cities * sizeof(int*));

    if (NULL != distances)
        for (int i = 0; i < map->number_of_cities; ++i)
            distances[i] = UINT_MAX;

    if (false == findRoute( map, starting_city, finishing_city, distances, predecessors, years)){
        free( distances);
        free( predecessors);
        free( years);
        return false;
    }

    if (false == addRoute( map, routeId, starting_city, finishing_city, predecessors)){
        free( distances);
        free( predecessors);
        free( years);
        return false;
    }

    free( distances);
    free( predecessors);
    free( years);
    return true;
}

bool newRouteByGivenCities( Map *map, unsigned routeId, const char *city1, char **cities_on_the_route,
        const unsigned *lengths_of_route_sections, const int *years_of_route_sections,  int length_of_arrays){

    if (NULL == map || 0 == routeId || routeId >= 1000 || NULL != map->array_of_routes[routeId] ||
        0 == length_of_arrays)
        return false;

    Route *route = malloc(sizeof(Route));
    if (NULL == route)
        return false;

    route->number_of_route_sections = length_of_arrays;
    route->array_of_route_sections = malloc(length_of_arrays * sizeof(Road*));
    if (NULL == route->array_of_route_sections){
        free(route);
        return false;
    }

    if (false == areParametersCorrectInFunctionNewRouteByGivenCities( map, city1, cities_on_the_route,
            lengths_of_route_sections, years_of_route_sections, length_of_arrays)){
        free(route->array_of_route_sections);
        free(route);
        return false;
    }

    City *my_city1 = findCityWithTheGivenName( map, city1);
    City *my_city2;
    Road *road;
    const char *name = city1;

    for (int i = 0; i < length_of_arrays; ++i){
        my_city2 = findCityWithTheGivenName( map, cities_on_the_route[i]);

        if (NULL != my_city1 && NULL != my_city2 && NULL != (road = findRoadToTheGivenCity
                ( my_city1->array_of_outgoing_roads, my_city1->number_of_outgoing_roads, my_city2->name))){

            if (road->construction_or_repair_year < years_of_route_sections[i])
                road->construction_or_repair_year = years_of_route_sections[i];
        }
        else{
            if (addRoad( map, name, cities_on_the_route[i], lengths_of_route_sections[i],
                    years_of_route_sections[i]) == false){
                free(route->array_of_route_sections);
                free(route);
                return false;
            }
        }

        my_city1 = findCityWithTheGivenName( map, name);
        my_city2 = findCityWithTheGivenName( map, cities_on_the_route[i]);

        road = findRoadToTheGivenCity( my_city1->array_of_outgoing_roads,
                my_city1->number_of_outgoing_roads, my_city2->name);
        route->array_of_route_sections[i] = road;

        name = cities_on_the_route[i];
        my_city1 = my_city2;
    }

    route->from_which_city = route->array_of_route_sections[0]->from_which_city;
    route->to_which_city = route->array_of_route_sections[length_of_arrays - 1]->to_which_city;

    map->array_of_routes[routeId] = route;

    if (false == addRouteToArraysOfRoutesPassingThroughThisRoad(route->array_of_route_sections,
            route->number_of_route_sections, routeId)){
        free(route->array_of_route_sections);
        free(route);
        return false;
    }

    return true;
}

bool extendRoute( Map *map, unsigned routeId, const char *city){

    if (NULL == map || routeId == 0 || routeId >= 1000 || NULL == map->array_of_routes[routeId])
        return false;

    City *city1 = findCityWithTheGivenName( map, city);
    Route *route = map->array_of_routes[ routeId];

    if (NULL == city1 || NULL == route || true == doesCityBelongToTheRoad( city1, route))
        return false;

    City *starting_city = route->from_which_city;
    City *finishing_city = route->to_which_city;

    unsigned int *distances1 = malloc( map->number_of_cities * sizeof(int*));
    int *predecessors1 = malloc( map->number_of_cities * sizeof(int*));
    int *years1 = malloc( map->number_of_cities * sizeof(int*));

    unsigned int *distances2 = malloc( map->number_of_cities * sizeof(int*));
    int *predecessors2 = malloc( map->number_of_cities * sizeof(int*));
    int *years2 = malloc( map->number_of_cities * sizeof(int*));

    if (NULL != distances1){
        for (int i = 0; i < map->number_of_cities; ++i)
            distances1[i] = UINT_MAX;

        for (int i = 0; i < route->number_of_route_sections ; ++i)
            distances1[route->array_of_route_sections[i]->to_which_city->position_on_map] = 0;
    }

    if (NULL != distances2) {
        for (int i = 0; i < map->number_of_cities; ++i)
            distances2[i] = UINT_MAX;

        distances2[ starting_city->position_on_map] = 0;

        for (int i = 0; i < route->number_of_route_sections; ++i)
            distances2[route->array_of_route_sections[i]->to_which_city->position_on_map] = 0;
    }

    bool find_route1 = findRoute( map, city1, starting_city, distances1, predecessors1, years1);
    bool find_route2 = findRoute( map, finishing_city, city1, distances2, predecessors2, years2);

    if (false == find_route1 && false == find_route2){
        free( distances1);
        free( predecessors1);
        free( years1);
        free( distances2);
        free( predecessors2);
        free( years2);
        return false;
    }
    if (false == find_route1){
        bool true_or_false = addExtensionAtTheEnd( map, route, city1, finishing_city, predecessors2, routeId);
        free( distances1);
        free( predecessors1);
        free( years1);
        free( distances2);
        free( predecessors2);
        free( years2);
        return true_or_false;
    }

    if (false == find_route2){
        bool true_or_false = addExtensionAtTheBeginning( map, route, city1, starting_city, predecessors1, routeId);
        free( distances1);
        free( predecessors1);
        free( years1);
        free( distances2);
        free( predecessors2);
        free( years2);
        return true_or_false;
    }

        bool true_or_false = addShorterExtension( map, route, city1, starting_city, finishing_city,
                distances1, predecessors1, years1, distances2, predecessors2, years2, routeId);
        free( distances1);
        free( predecessors1);
        free( years1);
        free( distances2);
        free( predecessors2);
        free( years2);
        return true_or_false;
}

bool removeRoad( Map *map, const char *city1, const char *city2){

    if (NULL == map)
        return false;

    City *our_city1 = findCityWithTheGivenName( map, city1);
    City *our_city2 = findCityWithTheGivenName( map, city2);

    if (NULL == our_city1 || NULL == our_city2)
        return false;

    Road *road1 = findRoadToTheGivenCity( our_city1->array_of_outgoing_roads, our_city1->number_of_outgoing_roads,
                                          city2);
    Road *road2 = findRoadToTheGivenCity( our_city2->array_of_outgoing_roads, our_city2->number_of_outgoing_roads,
                                          city1);
    if (NULL == road1 || NULL == road2)
        return false;

    int position1 = binSearchForArrayOfOutgoingRoads( city2, our_city1->array_of_outgoing_roads,
                                                               our_city1->number_of_outgoing_roads);
    int position2 = binSearchForArrayOfOutgoingRoads( city1, our_city2->array_of_outgoing_roads,
                                                               our_city2->number_of_outgoing_roads);

    if (0 == road1->number_of_routes_passing_through_this_road){
        removeRoadFromArray( our_city1, position1);
        removeRoadFromArray( our_city2, position2);
        free( road1->array_of_routes_passing_through_this_road);
        free( road2->array_of_routes_passing_through_this_road);
        free( road1);
        free( road2);
        return true;
    }

    unsigned **distances = malloc( road1->number_of_routes_passing_through_this_road * sizeof(int*));
    int **predecessors = malloc( road1->number_of_routes_passing_through_this_road * sizeof(int*));
    int **years = malloc( road1->number_of_routes_passing_through_this_road * sizeof(int*));

    if (NULL == distances || NULL == predecessors || NULL == years){
        free( distances);
        free( predecessors);
        free( years);
        return false;
    }

    if (setArrays( map, distances, predecessors, years, road1, our_city2) == false)
        return false;

    removeRoadFromArray( our_city1, position1);
    removeRoadFromArray( our_city2, position2);

    if (false == findRoutes( map, our_city1, our_city2, distances, predecessors, years,
            road1->number_of_routes_passing_through_this_road)){
        freeArrays( distances, predecessors, years, road1->number_of_routes_passing_through_this_road);
        addRoadToArray( our_city1, position1, road1);
        addRoadToArray( our_city2, position2, road2);
        return false;
    }

    for (int i = 0; i < road1->number_of_routes_passing_through_this_road; ++i) {
        if (false == completeRoutes(map, our_city1, our_city2, predecessors[i],
                road1->array_of_routes_passing_through_this_road[i])) {
            freeArrays( distances, predecessors, years, road1->number_of_routes_passing_through_this_road);
            addRoadToArray( our_city1, position1, road1);
            addRoadToArray( our_city2, position2, road2);
            return false;
        }
    }

    freeArrays( distances, predecessors, years, road1->number_of_routes_passing_through_this_road);
    free( road1->array_of_routes_passing_through_this_road);
    free( road2->array_of_routes_passing_through_this_road);
    free( road1);
    free( road2);

    return true;
}

bool removeRoute( Map *map, unsigned routeId){

    if (NULL == map || routeId == 0 || routeId >= 1000 || NULL == map->array_of_routes[routeId])
        return false;

    Road *road;

    for (int i = 0; i < map->array_of_routes[ routeId]->number_of_route_sections; ++i){
        road = map->array_of_routes[ routeId]->array_of_route_sections[i];
        int j;

        for (j = 0; j < road->number_of_routes_passing_through_this_road; ++j)
            if (road->array_of_routes_passing_through_this_road[j] == routeId)
                break;

        for (; j < road->number_of_routes_passing_through_this_road - 1; ++j)
            road->array_of_routes_passing_through_this_road[j] = road->array_of_routes_passing_through_this_road[j+1];

        road->number_of_routes_passing_through_this_road--;
    }

    free( map->array_of_routes[ routeId]->array_of_route_sections);
    free( map->array_of_routes[ routeId]);
    map->array_of_routes[ routeId] = NULL;
    return true;
}

char const *getRouteDescription( Map *map, unsigned routeId){

    if (NULL == map || routeId == 0 || routeId >= 1000){
        char *description = malloc(sizeof(char));

        if (NULL == description)
            return NULL;

        description[0] = '\0';
        return (const char *) description;
    }

    Route *route = map->array_of_routes[ routeId];

    if (NULL == route){
        char *description = malloc(sizeof(char));

        if (NULL == description)
            return NULL;

        description[0] = '\0';
        return (const char *) description;
    }

    char *route_description = malloc(20 * sizeof(char));

    if (NULL == route_description)
        return NULL;

    int length_of_route_description = 1;
    int length_of_allocated_memory_for_route_description = 20;
    route_description[0] = '\0';

    route_description = addToDescriptionNumberAndStartingCity( route_description, routeId,
            &length_of_allocated_memory_for_route_description, &length_of_route_description, route);

    if (NULL == route_description)
        return NULL;
    for (int i = 0; i < route->number_of_route_sections; ++i){
        route_description = addDescriptionOfOneRoad( route_description,
                &length_of_allocated_memory_for_route_description,
                &length_of_route_description, route->array_of_route_sections[i]);

        if (NULL == route_description)
            return NULL;
    }

    route_description = realloc(route_description, length_of_route_description * sizeof(char));

    return (const char *) route_description;
}