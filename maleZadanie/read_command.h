#ifndef READ_COMMAND_H_INCLUDED
#define READ_COMMAND_H_INCLUDED

char *GET_line();

int WHICH_command_is_it( char *line, char **history1, char **history2, uint64_t *energy_of_history);

#endif // READ_COMMAND_H_INCLUDED
