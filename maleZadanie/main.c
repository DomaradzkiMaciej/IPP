#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "macros.h"
#include "read_command.h"
#include "operations_on_tree.h"

void READ_command_and_do_it( quantum *root)
{
    uint64_t energy_of_history=0;
    int which_command_do=0;
    char *history1;
    char *history2;
    char *line;

    while( true)
    {
        history1=NULL;
        history2=NULL;
        line=NULL;
        energy_of_history=0;
        which_command_do=WRONG_COMMAND;

        line=GET_line();

        if(line==NULL)
        {
            return;
        }

        if( line[0]!='#' && line[0]!='\0')
        {
            which_command_do=WHICH_command_is_it( line, &history1, &history2, &energy_of_history);
            DO_command( root, history1, history2, energy_of_history, which_command_do);
        }

        free(history1);
        free(history2);
        free(line);
    }

}

int main()
{
    quantum *root = malloc( sizeof(quantum));

    if(root==NULL)
    {
        return 1;
    }

    root->son0=NULL;
    root->son1=NULL;
    root->son2=NULL;
    root->son3=NULL;
    root->quantum_history = malloc( 0*sizeof(char));

    READ_command_and_do_it( root);
    REMOVE_quantum_and_its_descendants( root);

    return 0;
}
