#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "macros.h"
#include "operations_on_tree.h"

bool IS_quantum_in_relation_with_other_quantum( quantum *pointer)
{
    if( pointer->next_in_relation!=pointer || pointer->pre_in_relation!=pointer)
    {
        return false;
    }
    else
    {
        return false;
    }
}

int COUNT_avg_energy( uint64_t energy1, uint64_t energy2)
{
    if( energy1%2==1 && energy2%2==1)
    {
        return energy1/2+energy2/2+1;
    }
    else
    {
        return energy1/2+energy2/2;
    }
}

void REMOVE_quantum( quantum *pointer)
{
    pointer->next_in_relation->pre_in_relation=pointer->pre_in_relation;
    pointer->pre_in_relation->next_in_relation=pointer->next_in_relation;

    if( IS_quantum_in_relation_with_other_quantum( pointer)==false)
    {
        free( pointer->pointer_on_energy);
    }

    free( pointer->quantum_history);
    free( pointer);
}

void REMOVE_quantum_and_its_descendants( quantum *pointer)
{
    if( pointer!=NULL)
    {
        REMOVE_quantum_and_its_descendants( pointer->son0);
        REMOVE_quantum_and_its_descendants( pointer->son1);
        REMOVE_quantum_and_its_descendants( pointer->son2);
        REMOVE_quantum_and_its_descendants( pointer->son3);

        REMOVE_quantum( pointer);
    }
}

quantum *ADD_quantum( char *history, int length_of_history)
{
    quantum *new_quantum = malloc( sizeof(quantum));
    new_quantum->quantum_history = malloc( length_of_history*sizeof(char));

    if( new_quantum==NULL || new_quantum->quantum_history==NULL)
    {
        exit(1);
    }

    new_quantum->pointer_on_energy=NULL;

    new_quantum->son0=NULL;
    new_quantum->son1=NULL;
    new_quantum->son2=NULL;
    new_quantum->son3=NULL;

    new_quantum->next_in_relation=new_quantum;
    new_quantum->pre_in_relation=new_quantum;

    for( int i=0; i<length_of_history; i++)
    {
        new_quantum->quantum_history[i]=history[i];
    }

    return new_quantum;
}

quantum *FIND_quantum_with_certain_history( quantum *root, char *history, bool will_quantum_be_deleted)
{
    quantum *pointer=root;
    quantum *pre_pointer=NULL;

    for( int i=0; history[i]!='\0'; i++)
    {
        if( pointer==NULL)
        {
            return NULL;
        }
        switch( history[i])
        {
            case '0':
                     pre_pointer=pointer;
                     pointer=pointer->son0;
                     break;
            case '1':
                     pre_pointer=pointer;
                     pointer=pointer->son1;
                     break;
            case '2':
                     pre_pointer=pointer;
                     pointer=pointer->son2;
                     break;
            case '3':
                     pre_pointer=pointer;
                     pointer=pointer->son3;
                     break;
            default:
                     return NULL;
        }
    }

    if( will_quantum_be_deleted==true)
    {
        if(pointer==pre_pointer->son0)
        {
            pre_pointer->son0=NULL;
        }
        if(pointer==pre_pointer->son1)
        {
            pre_pointer->son1=NULL;
        }
        if(pointer==pre_pointer->son2)
        {
            pre_pointer->son2=NULL;
        }
        if(pointer==pre_pointer->son3)
        {
            pre_pointer->son3=NULL;
        }
    }

    return pointer;
}

void DECLARE_history( quantum *root, char *history)
{
    quantum *pointer=root;
    quantum *pre_pointer=NULL;
    int i;

    for( i=0; history[i]!='\0'; i++)
    {
        if( pointer==NULL)
        {
            break;
        }
        switch( history[i])
        {
            case '0':
                    pre_pointer=pointer;
                    pointer=pointer->son0;
                    break;
            case '1':
                    pre_pointer=pointer;
                    pointer=pointer->son1;
                    break;
            case '2':
                    pre_pointer=pointer;
                    pointer=pointer->son2;
                    break;
            case '3':
                    pre_pointer=pointer;
                    pointer=pointer->son3;
                    break;
            default:
                    fprintf(stderr, "ERROR\n");
                    return;
        }
    }

    if(pointer!=NULL)
    {
        printf("OK\n");
        return;
    }

    for( ; history[i-1]!='\0'; i++)
    {
        pointer=ADD_quantum( history, i);
        switch( history[i-1])
        {
            case '0':
                    pre_pointer->son0=pointer;
                    pre_pointer=pointer;
                    break;
            case '1':
                    pre_pointer->son1=pointer;
                    pre_pointer=pointer;
                    break;
            case '2':
                    pre_pointer->son2=pointer;
                    pre_pointer=pointer;
                    break;
            case '3':
                    pre_pointer->son3=pointer;
                    pre_pointer=pointer;
                    break;
            default:
                    fprintf(stderr, "ERROR\n");
                    return;
        }
    }

    printf("OK\n");
}

void REMOVE_history( quantum *root, char *history)
{
    quantum *pointer;
    pointer=FIND_quantum_with_certain_history( root, history, true);
    REMOVE_quantum_and_its_descendants( pointer);
    printf("OK\n");
}

void VALID_history( quantum *root, char *history)
{
    quantum *pointer;
    pointer=FIND_quantum_with_certain_history( root, history, false);

    if( pointer!=NULL)
    {
        printf("YES\n");
    }
    else
    {
        printf("NO\n");
    }
}

void ENERGY_history_energy( quantum *root, char *history, uint64_t energy_of_history)
{
    quantum *pointer;
    pointer=FIND_quantum_with_certain_history( root, history, false);

    if( pointer!=NULL)
    {
        if( pointer->pointer_on_energy==NULL)
        {
            pointer->pointer_on_energy = malloc( sizeof(int));
        }

        *(pointer->pointer_on_energy)=energy_of_history;
        printf("OK\n");
    }
    else
    {
        fprintf(stderr, "ERROR\n");
    }
}

void ENERGY_history( quantum *root, char *history)
{
    quantum *pointer;
    pointer=FIND_quantum_with_certain_history( root, history, false);

    if( pointer!=NULL && pointer->pointer_on_energy!=NULL)
    {
        printf("%llu\n", *(pointer->pointer_on_energy));
    }
    else
    {
        fprintf(stderr, "ERROR\n");
    }
}

void EQUAL_history_a_history_b( quantum *root, char *history1, char *history2)
{
    quantum *pointer1;
    quantum *pointer2;
    pointer1=FIND_quantum_with_certain_history( root, history1, false);
    pointer2=FIND_quantum_with_certain_history( root, history2, false);

    if( pointer1==NULL || pointer2==NULL)
    {
        fprintf(stderr, "ERROR\n");
        return;
    }

    else if( pointer1==pointer2)
    {
        printf("OK\n");
        return;
    }

    else if( pointer1->pointer_on_energy==NULL && pointer2->pointer_on_energy==NULL)
    {
        fprintf(stderr, "ERROR\n");
        return;
    }

    else if( pointer1->pointer_on_energy==pointer2->pointer_on_energy)
    {
        printf("OK\n");
        return;
    }

    if( pointer1->pointer_on_energy==NULL)
    {
        pointer1->pointer_on_energy = malloc( sizeof(int));
        *(pointer1->pointer_on_energy)=*(pointer2->pointer_on_energy);
    }

    else if( pointer2->pointer_on_energy==NULL)
    {
        pointer2->pointer_on_energy = malloc( sizeof(int));
        *(pointer2->pointer_on_energy)=*(pointer1->pointer_on_energy);
    }


    *(pointer1->pointer_on_energy)=COUNT_avg_energy( *(pointer1->pointer_on_energy), *(pointer2->pointer_on_energy));
    quantum *next=pointer2->next_in_relation;
    free( pointer2->pointer_on_energy);

    pointer1->next_in_relation->pre_in_relation=pointer2->pre_in_relation;
    pointer2->pre_in_relation->next_in_relation=pointer1->next_in_relation;
    pointer1->next_in_relation=pointer2;
    pointer2->pre_in_relation=pointer1;

    while( next!=pointer2)
    {
        next->pointer_on_energy=pointer1->pointer_on_energy;
        next=next->next_in_relation;
    }

    printf("OK\n");

}

void DO_command( quantum *root, char *history1, char *history2, uint64_t energy_of_history, int which_command_do)
{
    switch( which_command_do)
    {
        case DECLARE:
                            DECLARE_history( root, history1);
                            break;
        case REMOVE:
                            REMOVE_history( root, history1);
                            break;
        case VALID:
                            VALID_history( root, history1);
                            break;
        case ENERGY_ENERGY:
                            ENERGY_history_energy( root, history1, energy_of_history);
                            break;
        case ENERGY:
                            ENERGY_history( root, history1);
                            break;
        case EQUAL:
                            EQUAL_history_a_history_b(root, history1, history2);
                            break;
        default:
                            fprintf(stderr, "ERROR\n");
    }
}
