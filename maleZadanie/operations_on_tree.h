#ifndef OPERATIONS_ON_TREE_H_INCLUDED
#define OPERATIONS_ON_TREE_H_INCLUDED

typedef struct quantum
{
    char *quantum_history;
    uint64_t *pointer_on_energy;

    struct quantum *son0;
    struct quantum *son1;
    struct quantum *son2;
    struct quantum *son3;

    struct quantum *pre_in_relation;
    struct quantum *next_in_relation;

} quantum;

void DO_command( quantum *root, char *history1, char *history2, uint64_t energy_of_history, int which_command_do);

void REMOVE_quantum_and_its_descendants( quantum *node);

#endif // OPERATIONS_ON_TREE_H_INCLUDED
