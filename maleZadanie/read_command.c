#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include "macros.h"
#include "read_command.h"

char *GET_line()
{
    char *line = malloc( 0*sizeof(char));
    char character;
    int length_of_line=0;

    for( int i=0; (character=getchar())!='\n'; i++)
    {
        if( character==EOF)
        {
            free( line);
            return NULL;
        }

        length_of_line++;
        line = realloc( line, length_of_line*(sizeof(char)));

        if( line==NULL)
        {
            exit(1);
        }

        line[i]=character;
    }

    length_of_line++;
    line = realloc( line, length_of_line*(sizeof(char)));

    if( line==NULL)
    {
        exit(1);
    }

    line[length_of_line-1]='\0';

    return line;
}

bool IS_next_character_space( char *line, int position_of_character)
{
    if( line[position_of_character]==' ')
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool IS_next_character_end_of_string( char *line, int position_of_character)
{
    if( line[position_of_character]=='\0')
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool IS_history_correct( char *line, int begin_point_of_history)
{
    int length_of_history=0;

    for( int i=begin_point_of_history; line[i]!=' ' && line[i]!='\0'; i++)
    {
        if( line[i]>'3' || line[i]<'0')
        {
            return false;
        }

        length_of_history++;
    }

    if(length_of_history==0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool IS_energy_correct( char *line, int begin_point_of_energy)
{
    int length_of_energy=0;
    char the_biggest_possible_energy[21]={'1','8','4','4','6','7','4','4','0','7','3','7','0','9','5','5','1','6','1','5','\0'};

    for( int i=begin_point_of_energy; line[i]!='\0'; i++)
    {
        if( line[i]>'9' || line[i]<'0')
        {
            return false;
        }

        length_of_energy++;
    }

    if( line[begin_point_of_energy]=='0' || length_of_energy==0 || length_of_energy>20)
    {
        return false;
    }

    else if( length_of_energy==20 && strcmp( line+begin_point_of_energy, the_biggest_possible_energy)>0)
    {
        return false;
    }

    else
    {
        return true;
    }
}

int FIND_history_and_its_length( char *line, int begin_point_of_history, char **history)
{
    int length_of_history=0;
    char *new_history;

    for( int i=begin_point_of_history; line[i]!=' ' && line[i]!='\0'; i++)
    {
        length_of_history++;
    }

    new_history = malloc( (length_of_history+1)*sizeof(char));

    if( line==NULL)
    {
        exit(1);
    }


    for( int i=begin_point_of_history; line[i]!=' ' && line[i]!='\0'; i++)
    {
        new_history[i-begin_point_of_history]=line[i];
    }

    new_history[length_of_history]='\0';
    *history=new_history;

    return length_of_history;
}

int FIND_energy_and_its_length( char *line, int begin_point_of_energy, uint64_t *energy_of_history)
{
    int length_of_energy=0;

    for( int i=begin_point_of_energy; line[i]!='\0'; i++)
    {
        length_of_energy++;
        *energy_of_history=10*(*energy_of_history)+line[i]-48;
    }

    return length_of_energy;
}

bool IS_it_DECLARE_history( char *line, char **history)
{
    int length_of_history=0;
    int position_of_first_char_of_history=8;

    if( strncmp( line, "DECLARE ", position_of_first_char_of_history )!=0)
    {
        return false;
    }

    else if( IS_history_correct( line, position_of_first_char_of_history)==false)
    {
        return false;
    }

    length_of_history=FIND_history_and_its_length( line, position_of_first_char_of_history, history);

    if( IS_next_character_end_of_string( line, position_of_first_char_of_history+length_of_history)==false)
    {
        return false;
    }

    else
    {
        return true;
    }
}

bool IS_it_REMOVE_history( char *line, char **history)
{
    int length_of_history=0;
    int position_of_first_char_of_history=7;

    if( strncmp( line, "REMOVE ", position_of_first_char_of_history )!=0)
    {
        return false;
    }

    else if( IS_history_correct( line, position_of_first_char_of_history)==false)
    {
        return false;
    }

    length_of_history=FIND_history_and_its_length( line, position_of_first_char_of_history, history);

    if( IS_next_character_end_of_string( line, position_of_first_char_of_history+length_of_history)==false)
    {
        return false;
    }

    else
    {
        return true;
    }
}

bool IS_it_VALID_history( char *line, char **history)
{
    int length_of_history=0;
    int position_of_first_char_of_history=6;

    if( strncmp( line, "VALID ", position_of_first_char_of_history )!=0)
    {
        return false;
    }

    else if( IS_history_correct( line, position_of_first_char_of_history)==false)
    {
        return false;
    }

    length_of_history=FIND_history_and_its_length( line, position_of_first_char_of_history, history);

    if( IS_next_character_end_of_string( line, position_of_first_char_of_history+length_of_history)==false)
    {
        return false;
    }

    else
    {
        return true;
    }
}

bool IS_it_ENERGY_history_energy( char *line, char **history, uint64_t *energy_of_history)
{
    int length_of_history=0;
    int length_of_energy=0;
    int position_of_first_char_of_history=7;
    int position_of_first_char_of_energy;

    if( strncmp( line, "ENERGY ", position_of_first_char_of_history )!=0)
    {
        return false;
    }

    else if( IS_history_correct( line, position_of_first_char_of_history)==false)
    {
        return false;
    }

    length_of_history=FIND_history_and_its_length( line, position_of_first_char_of_history, history);

    if( IS_next_character_space( line, position_of_first_char_of_history+length_of_history)==false)
    {
        return false;
    }

    position_of_first_char_of_energy=position_of_first_char_of_history+length_of_history+1;

    if( IS_energy_correct( line, position_of_first_char_of_energy)==false)
    {
        return false;
    }

    length_of_energy=FIND_energy_and_its_length( line, position_of_first_char_of_energy, energy_of_history);

    if( IS_next_character_end_of_string( line, position_of_first_char_of_energy+length_of_energy)==false)
    {
        return false;
    }

    else
    {
        return true;
    }

}

bool IS_it_ENERGY_history( char *line, char **history)
{
    int length_of_history=0;
    int position_of_first_char_of_history=7;

    if( strncmp( line, "ENERGY ", position_of_first_char_of_history )!=0)
    {
        return false;
    }

    else if( IS_history_correct( line, position_of_first_char_of_history)==false)
    {
        return false;
    }

    length_of_history=FIND_history_and_its_length( line, position_of_first_char_of_history, history);

    if( IS_next_character_end_of_string( line, position_of_first_char_of_history+length_of_history)==false)
    {
        return false;
    }

    else
    {
        return true;
    }
}

bool IS_it_EQUAL_history_a_history_b( char *line, char **history1, char **history2)
{
    int length_of_history1=0;
    int length_of_history2=0;
    int position_of_first_char_of_history1=6;
    int position_of_first_char_of_history2;

    if( strncmp( line, "EQUAL ", position_of_first_char_of_history1)!=0)
    {
        return false;
    }

    else if( IS_history_correct( line, position_of_first_char_of_history1)==false)
    {
        return false;
    }

    length_of_history1=FIND_history_and_its_length( line, position_of_first_char_of_history1, history1);

    if( IS_next_character_space( line, position_of_first_char_of_history1+length_of_history1)==false)
    {
        return false;
    }

    position_of_first_char_of_history2=position_of_first_char_of_history1+length_of_history1+1;

    if( IS_history_correct( line, position_of_first_char_of_history2)==false)
    {
        return false;
    }

    length_of_history2=FIND_history_and_its_length( line, position_of_first_char_of_history2, history2);

    if( IS_next_character_end_of_string( line, position_of_first_char_of_history2+length_of_history2)==false)
    {
        return false;
    }

    else
    {
        return true;
    }
}

int WHICH_command_is_it( char *line, char **history1, char **history2, uint64_t *energy_of_history)
{
    if( IS_it_DECLARE_history( line, history1)==true)
    {
        return DECLARE;
    }

    else if( IS_it_REMOVE_history( line, history1)==true)
    {
        return REMOVE;
    }

    else if( IS_it_VALID_history( line, history1)==true)
    {
        return VALID;
    }

    else if( IS_it_ENERGY_history( line, history1)==true)
    {
        return ENERGY;
    }

    else if( IS_it_ENERGY_history_energy( line, history1, energy_of_history)==true)
    {
        return ENERGY_ENERGY;
    }

    else if( IS_it_EQUAL_history_a_history_b( line, history1, history2)==true)
    {
        return EQUAL;
    }

    else
    {
        return WRONG_COMMAND;
    }
}
